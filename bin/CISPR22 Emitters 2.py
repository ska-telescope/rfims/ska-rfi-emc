# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 09:38:28 2018
This script compares the emission levels of CISPR22 to the required levels by SKA.

This is a worst case approach where it is considered that if there is no more information
on the test result of a CISPR 22 test, you can not lower the level of emissions from
an equipment by lowering the RBW in consideration. i.e. if the RBW used for measuring
was narrower than the RBW of the SKA std, you cannot lower the emissions when considering
the emissions with the new RBW.
The same works the other way, in the worst case approach, if the RBW of the standard is greater 
than the RBW used for measurement the emissions calculated with the broader RBW 
shall be affected by the relation of the RBWs.


@author: f.divruno
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from math import * 

os.system('clear')

#%%
# Define the frequency vector
Npoints = 200
# freq = np.array(range(50,25000)) #MHz
freq = np.logspace(np.log10(50),np.log10(25000),Npoints)

# Continuum limit line in PSD
SKA_Continuum_PSD =-17*np.log10(freq[freq<=2000])-192 #[dBm/Hz]
SKA_Continuum_PSD = np.append(SKA_Continuum_PSD,-249*np.ones(np.size(freq[freq>2000])))

# Spectral line treshold
SKA_Line_PSD = -17*np.log10(freq[freq<=2000])-177 #[dBm/Hz]
SKA_Line_PSD = np.append(SKA_Line_PSD,-234*np.ones(np.size(freq[freq>2000])))

plt.figure(num=1, figsize=(10, 7.5), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,SKA_Continuum_PSD, label = 'Continuum PSD')
plt.xlabel("frequency [MHz]")
plt.ylabel("Line PSD [dBm/Hz]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_PSD, label = 'Spectral Line PSD')
plt.grid(True,'both')
plt.title('SKA Power Spectral Density Limits [dBm/Hz] received by the antenna')
plt.legend(handles = [L1,L2])

#%%
#Convierte los valores de PSD recibida en densidad de potencia incidente a la antena.
# Continuum limit line in PSD
SKA_Continuum_rxS_Hz = SKA_Continuum_PSD + 10*np.log10(4*pi/((3e8)**2)) + 20*np.log10(freq*1e6)  #[dBm/(m2.Hz)]
SKA_Continuum_rxS = SKA_Continuum_rxS_Hz[freq<1e3] +10*np.log10(120e3) 
SKA_Continuum_rxS = np.append(SKA_Continuum_rxS , SKA_Continuum_rxS_Hz[freq>=1e3] +10*np.log10(120e3))

# Spectral line treshold
SKA_Line_rxS_Hz = SKA_Line_PSD + 10*np.log10(4*pi/(3e8**2)) + 20*np.log10(freq*1e6)  #[dBm/(m2.Hz)]
SKA_Line_rxS = SKA_Line_rxS_Hz + 10*np.log10(freq*1e6/100/1000)


plt.figure(num=1, figsize=(10, 7.5), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,SKA_Continuum_rxS_Hz, label = 'Continuum PSD')
plt.xlabel("frequency [MHz]")
plt.ylabel("Line PSD [dBm/(m2.Hz)]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_rxS_Hz, label = 'Spectral Line PSD')
plt.grid(True,'both')
plt.title('SKA Power Spectral Density Limits [dBm/(m2.Hz)] incident to the antenna')
plt.legend(handles = [L1,L2])



#%% Convert the PSDs to power

#Continuum limit threshold in received Power density
SKA_Continuum_rxS = SKA_Continuum_rxS_Hz[freq<1e3] +10*np.log10(120e3) 
SKA_Continuum_rxS = np.append(SKA_Continuum_rxS , SKA_Continuum_rxS_Hz[freq>=1e3] +10*np.log10(1e6))


# Spectral line treshold in received power density
SKA_Line_rxS = SKA_Line_rxS_Hz + 10*np.log10(freq*1e6/100/1000)


plt.figure(num=2, figsize=(10, 7.5), dpi=80)
L1, = plt.plot(freq,SKA_Continuum_rxS, label = "Continuum scaled with CISPR RBWs")
plt.xlabel("frequency [MHz]")
plt.ylabel("Power density [dBm/m2]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_rxS, label = "Spectral Line scaled with SKA RBWs ")
plt.grid(True,'both')
plt.title('SKA incident power density to antennas ')
plt.legend(handles = [L1,L2])




#%% Convert the Pwer density to electric fiend received by the antennas 

#Continuum limit threshold in received Power density
SKA_Continuum_rxE = SKA_Continuum_rxS + 115.76
SKA_Continuum_txE = SKA_Continuum_rxE + 20*np.log10(1/3) 
SKA_Continuum_txE2 = SKA_Continuum_PSD[freq<1e3] +10*np.log10(120e3) + 20*np.log10(1/3) + 20*np.log10(freq[freq<1000]*1e6) - 42.79
SKA_Continuum_txE2 = np.append(SKA_Continuum_txE2 , SKA_Continuum_PSD[freq>=1e3] +10*np.log10(1e6) + 20*np.log10(1/3) + 20*np.log10(freq[freq>=1000]*1e6) - 42.79)


# Spectral line treshold in received power density
SKA_Line_rxE = SKA_Line_rxS + 115.76
SKA_Line_txE = SKA_Line_rxE + 20*np.log10(1/3)
SKA_Line_txE2 = SKA_Line_PSD + 10*np.log10(0.001/100*freq*1e6) + 20*np.log10(1/3) + 20*np.log10(freq*1e6) - 42.79


plt.figure(num=2, figsize=(10, 7.5), dpi=80)
L1, = plt.plot(freq,SKA_Continuum_txE, label = "Continuum")
plt.xlabel("frequency [MHz]")
plt.ylabel("Electric field [dBuV/m]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_txE, label = "Spectral Line")
L3, = plt.plot(freq,SKA_Continuum_txE2, label = "Continuum 2")
L4, = plt.plot(freq,SKA_Line_txE2, label = "Spectral Line2")
plt.grid(True,'both')
plt.title('SKA incident electric field to antennas [dBuV/m]')
plt.legend(handles = [L1,L2,L3,L4])




#%% ------------------  Conversion from CISPR22 emitter to RX antenna  ------------------  ##
# This section assumes that the FSPL is zero and the shielding is zero, so calculates the radiated 
#power from a source as if it would couple entirely to the RX antenna.

### ---------------------- CISPR 22 limits -------------------------------------------------------- ##
CISPR22_f = np.array([30,230,230,1000,1000,30000,3000,6000,25000])
CISPR22 = np.array([40, 40, 47, 47, 76,76,80, 80, 80]) #dBuV/m

CISPR22_i = np.interp(freq,CISPR22_f,CISPR22)

L = 3 # distance of the measurement antenna to the EUT.

# because CISPR22 is measured @10m under 1 GHZ, convert to "L" m values.
#CISPR22_i[freq<1000] = CISPR22_i[freq<1000]+10*np.log10(10) - 10*np.log10(L)
CISPR22_i[freq<1000] = CISPR22_i[freq<1000]+10*np.log10(10) - 10*np.log10(L)  
# because CISPR22 is measured @3m above 1 GHZ, convert to "L" m values.
CISPR22_i[freq>=1000] = CISPR22_i[freq>=1000]+10*np.log10(3) - 10*np.log10(L) #[dBuV/m]


#  Ptx = E^2/377*(4*pi*R^2)
#  Ptx = E - 10*log10(377) + 10*log10(4*pi*R^2)
CISPR_Prx = CISPR22_i - 10*np.log10(377) + 10*np.log10(4*pi*L*L) - 120  + 30# the 
# -120 is to pass from dBuV to dBV. The +30 is to get to dBm


plt.figure(num=3, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Power [dBm/RBW]")
plt.xscale('log')
L1, = plt.plot(freq,SKA_Line_Pow, label = 'Spectral Line Rx power scaled with SKA std RBW')
L2, = plt.plot(freq,SKA_Continuum_Pow, label = 'Continuum Rx power scaled with SKA std RBW')
L3, = plt.plot(freq, CISPR_Prx, label = 'CISPR22 Tx EIRP')
plt.grid(True,'both')
plt.title('SKA Received power Limits, calculated from PSD with RBWs [dBm/RBW]')
plt.legend(handles = [L1,L2,L3])


#%% Comparision for Spectrum line limits

plt.figure(num=4, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_Pow, label = "SKA spectral line limit in the receiver antenna")
plt.grid(True,'both')
plt.title('SKA Received power Limits for spectral line')
L3, = plt.plot(freq,CISPR_Prx, label = "1 CISPR power in the receiver antenna without att and shielding (RBW=120kHz/1MHz)")
plt.legend(handles=[L2,L3])


#%% Comparison with continuum 

# Pasa los valores de potencia a PSD
CISPR_Prx_psd = CISPR_Prx[freq<1e3] - 10*np.log10(120e3)  
CISPR_Prx_psd = np.append(CISPR_Prx_psd,CISPR_Prx[freq>=1e3] - 10*np.log10(1e6))

# Compare the RBW of the SKA std and the CISPR std, where the SKA RBW is greater,
# compute the difference to then convert the PSD to Power.

RBW_CISPR = 120e3*np.ones(np.size(freq[freq<1e3])) #CISPR22 RBW
RBW_CISPR = np.append(RBW_CISPR, 1e6*np.ones(np.size(freq[freq>=1e3])))

RBW_SKA = 1/100*freq*1e6 #SKA std RBW

RBW_difference = -10*np.log10(RBW_CISPR) + 10*np.log10(RBW_SKA)
RBW_difference[RBW_difference<0] = 0 #zero if CISPR22 RBW is greater than SKA RBW



plt.figure(num=5, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm]")
plt.xscale('log')
L1, = plt.plot(freq,SKA_Continuum_Pow, label = "SKA continuum limit in the receiver antenna")
plt.grid(True,'both')
plt.title('SKA Received power Limits for Continuum')
L2, = plt.plot(freq,CISPR_Prx + RBW_difference, label = "RX power of 1 CISPR emitter no att no shielding (RBW corrected for SKA RBW/CISPR RBW)")
plt.legend(handles=[L1,L2])


#%% Attenuation necesarios para Continuum y Spectral line

Att_Continuum = CISPR_Prx + RBW_difference - SKA_Continuum_Pow
#Att_Continuum = CISPR_Prx  - SKA_Continuum_Pow
Att_Line = CISPR_Prx - SKA_Line_Pow


plt.figure(num=6, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L6_1, = plt.plot(freq,Att_Continuum, label = "SKA Continuum")
plt.grid(True,'both')
plt.title('Necessary attenuation for 1 CISPR22 emitter to comply with SKA stds')
L6_2, = plt.plot(freq,Att_Line, label = "SKA Spectral Line")
plt.legend(handles=[L6_1,L6_2])


Min_att = np.maximum(Att_Continuum,Att_Line)

plt.figure(num=7, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Min_att, label = "Max att needed")
plt.grid(True,'both')
plt.title('Minimum attenuation needed for 1 CISPR22 emitter to comply with SKA stds')
plt.legend(handles=[L1,L2])


#%% Attenuation because of the distance  and number
'''
N = 3000
multiple_emmiters = 10*np.log10(N)

# Rx and TX gain is considered 0 dBi
d = 1500 #distance between tx and rx in m

# free space loss = -27.55 + 20*log10(f_MHz) + 20*log10(d_m) + extra_att dB
extra_att = 10 # from RALI MS32.
FSPL = -27.55 + 20*np.log10(freq) + 20*np.log10(d) + extra_att 

Att_needed = Max_att - FSPL + multiple_emmiters

plt.figure(num=8, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Att_needed, label = "Max att needed")
plt.grid(True,'both')
plt.title('Attenuation needed for ' + str(N) + ' CISPR22 emitters at ' + str(d) + 'm  to comply with SKA stds')
'''


#%%


'''
This section calculates the same thing but scales the CISPR22 emissions by the ratio of RBWs.
'''

#%%

#%% ------------------  Conversion from CISPR22 emitter to RX antenna  ------------------  ##
# This section assumes that the FSPL is zero and the shielding is zero, so calculates the radiated 
#power from a source as if it would couple entirely to the RX antenna.

### ---------------------- CISPR 22 limits -------------------------------------------------------- ##
CISPR22_f = np.array([30,230,230,1000,1000,30000,3000,6000,25000])
CISPR22 = np.array([40, 40, 47, 47, 76,76,80, 80, 80]) #dBuV/m

CISPR22_i = np.interp(freq,CISPR22_f,CISPR22)
CISPR22_psd = CISPR22_i

L = 3 # distance of the measurement antenna to the EUT.

# because CISPR22 is measured @10m under 1 GHZ, convert to "L" m values.
#CISPR22_i[freq<1000] = CISPR22_i[freq<1000]+10*np.log10(10) - 10*np.log10(L)
CISPR22_psd[freq<1000] = CISPR22_i[freq<1000]+10*np.log10(10) - 10*np.log10(L) -10*np.log10(120e3) 
# because CISPR22 is measured @3m above 1 GHZ, convert to "L" m values.
CISPR22_psd[freq>=1000] = CISPR22_i[freq>=1000]+10*np.log10(3) - 10*np.log10(L) -10*np.log10(1e6)


#  Ptx = E^2/377*(4*pi*R^2)
#  Ptx = E - 10*log10(377) + 10*log10(4*pi*R^2)
CISPR_psd = CISPR22_psd - 10*np.log10(377) + 10*np.log10(4*pi*L*L) - 120  + 30# the 
# -120 is to pass from dBuV to dBV, Ptx is in dBW . The +30 is to get to dBm

CISPR_RBW_Continuum = CISPR_psd + 10*np.log10(1/100*freq*1e6)
CISPR_RBW_Line = CISPR_psd + 10*np.log10(0.001/100*freq*1e6)



plt.figure(num=9, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm/RBW]")
plt.xscale('log')
L1, = plt.plot(freq,SKA_Line_Pow, label = 'Spectral Line scaled with SKA std RBW')
L2, = plt.plot(freq,SKA_Continuum_Pow, label = 'Continuum scaled with SKA std RBW')
L3, = plt.plot(freq, CISPR_RBW_Continuum, label = 'CISPR22 scaled with ratio of RBWs for Continuum')
L4, = plt.plot(freq, CISPR_RBW_Line, label = 'CISPR22_Prx scaled with ratio of RBWs for Line')
plt.grid(True,'both')
plt.title('SKA Received power Limits, calculated from PSD with RBWs [dBm/RBW]')
plt.legend(handles = [L1,L2,L3,L4])


#%% Comparision for Spectrum line limits

plt.figure(num=10, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_Pow, label = "SKA spectral line limit in the receiver antenna")
plt.grid(True,'both')
plt.title('SKA Received power Limits for spectral line')
L3, = plt.plot(freq,CISPR_RBW_Line, label = "1 CISPR power in the receiver antenna without att and shielding (scaled per ratio RBW)")
plt.legend(handles=[L2,L3])


#%% Comparison with continuum 


plt.figure(num=11, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm]")
plt.xscale('log')
L1, = plt.plot(freq,SKA_Continuum_Pow, label = "SKA continuum limit in the receiver antenna")
plt.grid(True,'both')
plt.title('SKA Received power Limits for Continuum')
L2, = plt.plot(freq,CISPR_RBW_Continuum, label = "1 CISPR power in the receiver antenna without att and shielding (scaled per ratio RBW)")
plt.legend(handles=[L1,L2])


#%% Attenuation necesarios para Continuum y Spectral line

Att_Continuum = CISPR_RBW_Continuum - SKA_Continuum_Pow
#Att_Continuum = CISPR_Prx  - SKA_Continuum_Pow
Att_Line = CISPR_RBW_Line - SKA_Line_Pow


plt.figure(num=12, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Att_Continuum, label = "SKA Continuum")
plt.grid(True,'both')
plt.title('Necessary attenuation for 1 CISPR22 emitter to comply with SKA stds (scaled CISPR per ratio RBW)')
L2, = plt.plot(freq,Att_Line, label = "SKA Spectral Line")
plt.legend(handles=[L1,L2])


Min_att_RBW_scaled = np.maximum(Att_Continuum,Att_Line)

plt.figure(num=13, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Min_att_RBW_scaled, label = "Max att needed")
plt.grid(True,'both')
plt.title('Min attenuation needed for 1 CISPR22 emitter to comply with SKA stds (scaled CISPR per ratio RBW)')
plt.legend(['Min attenuation worst case','Min attenuation with RBW ratio for CISPR'])


#%% Attenuation because of the distance  and number
'''
N = 3000
multiple_emmiters = 10*np.log10(N)

# Rx and TX gain is considered 0 dBi
d = 1500 #distance between tx and rx in m

# free space loss = -27.55 + 20*log10(f_MHz) + 20*log10(d_m) + extra_att dB
extra_att = 10 # from RALI MS32.
FSPL = -27.55 + 20*np.log10(freq) + 20*np.log10(d) + extra_att 

Att_needed = Max_att - FSPL + multiple_emmiters

plt.figure(num=14, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Att_needed, label = "Max att needed")
plt.grid(True,'both')
plt.title('Attenuation needed for ' + str(N) + ' CISPR22 emitters at ' + str(d) + 'm  to comply with SKA stds (scaled CISPR per ratio RBW')
'''

#%% Comparison with and without scaling

plt.figure(num=15, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Min_att, label = "Worst case")
plt.grid(True,'both')
plt.title('Minimum attenuation for 1 CISPR22 emitter to comply with SKA stds')
L2, = plt.plot(freq,Min_att_RBW_scaled, label = "RBW scaling")
plt.legend(handles=[L1,L2])


