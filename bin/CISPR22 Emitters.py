# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 09:38:28 2018
THis script compares the emission levels of CISPR22 to the required levels by SKA.

This is a worst case approach where it is considered that if there is no more information
on the test result of a CISPR 22 test, you can not lower the level of emissions from
an equipment by lowering the RBW in consideration. i.e. if the RBW used for 




@author: f.divruno
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from math import * 

os.system('clear')

#%%
# Define the frequency vector
# freq = np.array(range(50,25000)) #MHz
freq = np.logspace(np.log10(50),np.log10(25000),200)

# Continuum limit line in PSD
SKA_Continuum_PSD =-17*np.log10(freq[freq<=2000])-192 #[dBm/Hz]
SKA_Continuum_PSD = np.append(SKA_Continuum_PSD,-249*np.ones(np.size(freq[freq>2000])))

# Spectral line treshold
SKA_Line_PSD = -17*np.log10(freq[freq<=2000])-177 #[dBm/Hz]
SKA_Line_PSD = np.append(SKA_Line_PSD,-234*np.ones(np.size(freq[freq>2000])))

plt.figure(num=1, figsize=(10, 7.5), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,SKA_Continuum_PSD, label = 'Continuum PSD')
plt.xlabel("frequency [MHz]")
plt.ylabel("Line PSD [dBm/Hz]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_PSD, label = 'Spectral Line PSD')
plt.grid(True,'both')
plt.title('SKA Power Spectral Density Limits [dBm/Hz]')
plt.legend(handles = [L1,L2])



#%% Convert the PSDs to power

#Continuum limit line in Received Power

SKA_Continuum_Pow = SKA_Continuum_PSD + 10*np.log10(1/100*freq*1e6) #[dBm]

# Spectral line treshold in received power
SKA_Line_Pow = SKA_Line_PSD + 10*np.log10(0.001/100*freq*1e6) #[dBm]

plt.figure(num=2, figsize=(10, 7.5), dpi=80)
plt.plot(freq,SKA_Continuum_Pow)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm/RBW]")
plt.xscale('log')
plt.plot(freq,SKA_Line_Pow)
plt.grid(True,'both')
plt.title('SKA Received power Limits [dBm/RBW] ')



#%% ------------------  Conversion from CISPR22 emitter to RX antenna  ------------------  ##
# This section assumes that the FSPL is zero and the shielding is zero, so calculates the radiated 
#power from a source as if it would couple entirely to the RX antenna.

### ---------------------- CISPR 22 limits -------------------------------------------------------- ##
CISPR22_f = np.array([30,230,230,1000,1000,30000,3000,6000,25000])
CISPR22 = np.array([40, 40, 47, 47, 76,76,80, 80, 80]) #dBuV/m

CISPR22_i = np.interp(freq,CISPR22_f,CISPR22)

L = 3 # distance of the measurement antenna to the EUT.

# because CISPR22 is measured @10m under 1 GHZ, convert to "L" m values.
#CISPR22_i[freq<1000] = CISPR22_i[freq<1000]+10*np.log10(10) - 10*np.log10(L)
CISPR22_i[freq<1000] = CISPR22_i[freq<1000]+10*np.log10(10) - 10*np.log10(L)  # 15 for compensating the QP detector
# because CISPR22 is measured @3m above 1 GHZ, convert to "L" m values.
CISPR22_i[freq>=1000] = CISPR22_i[freq>=1000]+10*np.log10(3) - 10*np.log10(L)


#  Ptx = E^2/377*(4*pi*R^2)
#  Ptx = E - 10*log10(377) + 10*log10(4*pi*R^2)
CISPR_Prx = CISPR22_i - 10*np.log10(377) + 10*np.log10(4*pi*L*L) - 120  + 30# the 
# -120 is to pass from dBuV to dBV, Ptx is in dBW . The +30 is to get to dBm


plt.figure(num=3, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm/RBW]")
plt.xscale('log')
L1, = plt.plot(freq,SKA_Line_Pow, label = 'Spectral Line scaled with SKA std RBW')
L2, = plt.plot(freq,SKA_Continuum_Pow, label = 'Continuum scaled with SKA std RBW')
L3, = plt.plot(freq, CISPR_Prx, label = 'CISPR22_Prx')
plt.grid(True,'both')
plt.title('SKA Received power Limits, calculated from PSD with RBWs [dBm/RBW]')
plt.legend(handles = [L1,L2,L3])


#%% Comparision for Spectrum line limits

plt.figure(num=4, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_Pow, label = "SKA spectral line limit in the receiver antenna")
plt.grid(True,'both')
plt.title('SKA Received power Limits for spectral line')
L3, = plt.plot(freq,CISPR_Prx, label = "1 CISPR power in the receiver antenna without att and shielding (RBW=120kHz/1MHz)")
plt.legend(handles=[L2,L3])


#%% Comparison with continuum 

# Pasa los valores de potencia a PSD
CISPR_Prx_psd = CISPR_Prx[freq<1e3] - 10*np.log10(120e3)  
CISPR_Prx_psd = np.append(CISPR_Prx_psd,CISPR_Prx[freq>=1e3] - 10*np.log10(1e6))

# Compare the RBW of the SKA std and the CISPR std, where the SKA RBW is greater,
# compute the difference to then convert the PSD to Power.

RBW_CISPR = 120e3*np.ones(np.size(freq[freq<1e3])) #CISPR22 RBW
RBW_CISPR = np.append(RBW_CISPR, 1e6*np.ones(np.size(freq[freq>=1e3])))

RBW_SKA = 1/100*freq*1e6 #SKA std RBW

RBW_difference = -10*np.log10(RBW_CISPR) + 10*np.log10(RBW_SKA)
RBW_difference[RBW_difference<0] = 0 #zero if CISPR22 RBW is greater than SKA RBW



plt.figure(num=5, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Line Power [dBm]")
plt.xscale('log')
L1, = plt.plot(freq,SKA_Continuum_Pow, label = "SKA continuum limit in the receiver antenna")
plt.grid(True,'both')
plt.title('SKA Received power Limits for Continuum')
L2, = plt.plot(freq,CISPR_Prx + RBW_difference, label = "RX power of 1 CISPR emitter no att no shielding (RBW corrected for SKA RBW/CISPR RBW)")
plt.legend(handles=[L1,L2])


#%% Attenuation necesarios para Continuum y Spectral line

Att_Continuum = CISPR_Prx + RBW_difference - SKA_Continuum_Pow
#Att_Continuum = CISPR_Prx  - SKA_Continuum_Pow
Att_Line = CISPR_Prx - SKA_Line_Pow


plt.figure(num=6, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Att_Continuum, label = "SKA Continuum")
plt.grid(True,'both')
plt.title('Necessary attenuation for 1 CISPR22 emitter to comply with SKA stds')
L2, = plt.plot(freq,Att_Line, label = "SKA Spectral Line")
plt.legend(handles=[L1,L2])


Max_att = np.maximum(Att_Continuum,Att_Line)

plt.figure(num=7, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Max_att, label = "Max att needed")
plt.grid(True,'both')
plt.title('Maximum attenuation needed for 1 CISPR22 emitter to comply with SKA stds')
#plt.legend(handles=[L1,L2])


#%% Attenuation because of the distance  and number

N = 10
multiple_emmiters = 10*np.log10(N)

# Rx and TX gain is considered 0 dBi
d = 2000 #distance between tx and rx in m

# free space loss = -27.55 + 20*log10(f_MHz) + 20*log10(d_m) + extra_att dB
extra_att = 10 # from RALI MS32.
FSPL = -27.55 + 20*np.log10(freq) + 20*np.log10(d) + extra_att 

Att_needed = Max_att - FSPL + multiple_emmiters

plt.figure(num=8, figsize=(10, 7.5), dpi=80)
plt.xlabel("frequency [MHz]")
plt.ylabel("Att [dB]")
plt.xscale('log')
L1, = plt.plot(freq,Att_needed, label = "Max att needed")
plt.grid(True,'both')
plt.title('Attenuation needed for ' + str(N) + ' CISPR22 emitters at ' + str(d) + 'm  to comply with SKA stds')



