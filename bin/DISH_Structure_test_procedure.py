# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 12:03:11 2018

Translate SKA requirements from RX to TX side.


@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt
from math import * 
import matplotlib

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
matplotlib.rc('font', **font)



#%% Define the frequency vector in MHz
Npoints = 100 # Number of points to represent the frequency
fmin = 50
fmax = 20e3
freq = np.logspace(np.log10(fmin),np.log10(fmax),Npoints) #[MHz]
#freq = np.linspace(fmin,fmax,Npoints) #[MHz]


#%% distance from victim (Dish or station) to culprit. in metre
r2 = [500,500,500,500]
r2_f = [fmin,300, 300, fmax] #Frequency of the different distance values
r2 = np.interp(freq,r2_f,r2)
#r2 = 1 # Uncomment this for fixed distance for all frequencies


#%% Shielding of the building / enclosure [dB]
shield = [145,145,90,90]# distance of the measurement antenna to the EUT.
shield_MK = [80,80,60,60]# distance of the measurement antenna to the EUT.
shield_f = [fmin, 13.8e3,13.8e3,fmax]
shield_MK_f = [fmin, 3e3,3e3,fmax]
shield = np.interp(freq,shield_f,shield)
shield_MK = np.interp(freq,shield_MK_f,shield_MK)

#shield = shield_MK #Uncomment for fixed shielding for all frequencies
#shield = 0*np.ones(Npoints) # Uncomment for fixed shielding for all frequencies

plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,shield, label = 'Shielding levels (SKA DISH)')
L2, = plt.plot(freq,shield_MK, label = 'Shielding levels (Meerkat DISH)')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dB]")
plt.xscale('log')
plt.grid(True,'both')
plt.legend()
plt.title('Shielding level')


# Extra attenuation in FSPL according to RALI32.
RALI32_extra_att = 0#*(r2>100)

#%% Number of equipments considered inside the shielded building
N = 1

#Budget portion for this particular system
Budget = 100 #in percent


#%% EMC standards 

### ---------------------- CISPR 22 class A limits -------------------------------------------------------- ##

CISPR22_f = np.array([30,230,230,1000,1000,3000,3000,6000,fmax])
CISPR22 = np.array([40, 40, 47, 47, 76,76,80, 80, 80]) #dBuV/m
#CISPR22 = np.array([50, 50, 57, 57, 76,76,80, 80, 80]) #dBuV/m with QP consideration
RBW_meas = [120e3, 120e3, 1e6, 1e6]
RBW_meas_f = [fmin, 1000, 1000, fmax] #[MHz]
RBW_meas = np.interp(freq, RBW_meas_f, RBW_meas)
#RBW_meas = freq/100*1e6 # Uncomment for fixed RBW, or function of F
r1 = [1,1,1,1]
r1_f = [fmin, 1e3,1e3,fmax] #Frequency of the different distance values
r1 = np.interp(freq,r1_f,r1)
#r1 = 1 #Uncomment this for fixed distance for all frequencies

Std_name = 'CISPR22 class A'


### ---------------------- CISPR 22 class A limits modified above 1 GHz-------------------------------------------------------- ##
'''
CISPR22_f = np.array([30,230,230,1000,1000,3000,3000,6000,fmax])
CISPR22 = np.array([40, 40, 47, 47, 47,47,47, 47, 47]) #dBuV/m
RBW_meas = [120e3, 120e3, 1e6, 1e6]
RBW_meas_f = [fmin, 1000, 1000, fmax] #[MHz]
RBW_meas = np.interp(freq, RBW_meas_f, RBW_meas)
RBW_meas = freq/100*1e6 # Uncomment for fixed RBW, or function of F
Std_name = 'CISPR22 class A modified after 1 GHz'
'''

### ---------------------- CISPR 22 class B limits -------------------------------------------------------- ##
'''
CISPR22_f = np.array([30,230,230,1000,1000,3000,3000,6000,fmax])
CISPR22 = np.array([30, 30, 37, 37, 66,66,70, 70, 70]) #dBuV/m
RBW_meas = [120e3, 120e3, 1e6, 1e6]
RBW_meas_f = [fmin, 1000, 1000, fmax] #[MHz]
RBW_meas = np.interp(freq, RBW_meas_f, RBW_meas)
RBW_meas = freq/100*1e6 # Uncomment for fixed RBW, or function of F
Std_name = 'CISPR22 class B'
'''
### ---------------------- CISPR 11 class A limits modified above 1 GHz-------------------------------------------------------- ##
'''
CISPR22_f = np.array([30,230,230,1000,1000,3000,3000,6000,fmax])
CISPR22 = np.array([71.7, 60, 60, 60, 60,60,60, 60, 60]) #dBuV/m
RBW_meas = [120e3, 120e3, 1e6, 1e6]
RBW_meas_f = [fmin, 1000, 1000, fmax] #[MHz]
RBW_meas = np.interp(freq, RBW_meas_f, RBW_meas)
RBW_meas = freq/100*1e6 # Uncomment for fixed RBW, or function of F
Std_name = 'CISPR11 class A'
'''

### ---------------------- Mil-461 limit line-------------------------------------------------------- ##
'''
CISPR22_f = np.array([0.1,100,18e3,20e3])
CISPR22 = np.array([24, 24, 69, 69])#dBuV/m
RBW_meas = [10e3, 100e3, 1e6, 1e6]
RBW_meas_f = [fmin, 1000, 1000, fmax] #[MHz]
RBW_meas = np.interp(freq, RBW_meas_f, RBW_meas)
#RBW_meas = freq/100*1e6 # Uncomment for fixed RBW, or function of F
r1 = 1 #Uncomment this for fixed distance for all frequencies
Std_name = 'MIL-STD-461 Ground applications'
'''

### ---------------------- Mil-461 limit line-------------------------------------------------------- ##
'''
CISPR22_f = np.array([0.1,100,18e3,20e3])
CISPR22 = np.array([24, 24, 69, 69])-20 #dBuV/m
RBW_meas = [10e3, 100e3, 1e6, 1e6]
RBW_meas_f = [fmin, 1000, 1000, fmax] #[MHz]
RBW_meas = np.interp(freq, RBW_meas_f, RBW_meas)
#RBW_meas = freq/100*1e6 # Uncomment for fixed RBW, or function of F
r1 = 1 #Uncomment this for fixed distance for all frequencies
Std_name = 'MIL-STD-461(-20dB) Ground applications'
'''



CISPR22_i = np.interp(np.log10(freq),np.log10(CISPR22_f),CISPR22)
#CISPR22_i = np.interp(freq,(CISPR22_f),CISPR22)



### ---------------------- CISPR 22 ---------------------------------------------------------------------- ##


#%% SKA Standard Limits definitions

# Continuum limit line in PSD
SKA_Continuum_PSD =-17*np.log10(freq[freq<2000])-192 #[dBm/Hz]
SKA_Continuum_PSD = np.append(SKA_Continuum_PSD,-249*np.ones(np.size(freq[freq>=2000])))
RBW_Continuum = freq*1e6*1/100

## Continuum limit line in PSD for SARAS Levels
#SKA_Continuum_PSD =-17.2708*np.log10(freq[freq<2000])-197.0714 #[dBm/Hz]
#SKA_Continuum_PSD = np.append(SKA_Continuum_PSD,-253.8661*np.ones(np.size(freq[freq>=2000])))
#RBW_Continuum = freq*1e6*1/100

# Spectral line treshold
SKA_Line_PSD = -17*np.log10(freq[freq<2000])-177 #[dBm/Hz]
SKA_Line_PSD = np.append(SKA_Line_PSD,-234*np.ones(np.size(freq[freq>=2000])))
RBW_Line = freq*1e6*0.001/100



plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,SKA_Continuum_PSD, label = 'Continuum PSD')
plt.xlabel("frequency [MHz]")
plt.ylabel("PSD [dBm/Hz]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_PSD, label = 'Spectral Line PSD')
plt.grid(True,'both')
plt.title('SKA Power Spectral Density Limits [dBm/Hz] received by the antenna')
plt.legend(handles = [L1,L2])


#%% Received power in antenna with the RBW in the standard

SKA_Continuum_dBm = SKA_Continuum_PSD + 10*np.log10(RBW_Continuum)
SKA_Line_dBm = SKA_Line_PSD + 10*np.log10(RBW_Line)


plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,SKA_Continuum_dBm, label = 'Continuum Rx power')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBm]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_dBm, label = 'Spectral Line Rx power')
plt.grid(True,'both')
plt.title('SKA Power Limits [dBm] received by the antenna')
plt.legend(handles = [L1,L2])




#%% Apply the friis equation to get the transmitted power at a certain distance

FSPL = 20*np.log10(freq) + 20*np.log10(r2) - 27.56 + RALI32_extra_att

plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,FSPL, label = ' Free Space Prop Loss ')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dB]")
plt.xscale('log')
plt.grid(True,'both')
plt.title('FSPL at r2 distance')
plt.legend()


plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,SKA_Continuum_PSD + FSPL + shield, label = 'Continuum Tx power per Hz')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBm/Hz]")
plt.xscale('log')
L2, = plt.plot(freq,SKA_Line_PSD + FSPL + shield, label = 'Spectral Line Tx power per Hz')
plt.grid(True,'both')
plt.title('SKA Power density Limits [dBm/Hz] transmitted by a culprit at r2 distance')
plt.legend()


Ptx_Continuum = SKA_Continuum_dBm  + FSPL + shield
Ptx_Line = SKA_Line_dBm  + FSPL + shield


# This result is the power that the Tx is allowed to radiate measured with the same RBW as the one used by the Rx antenna.
plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,Ptx_Continuum, label = 'Continuum Tx power')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBm]")
plt.xscale('log')
L2, = plt.plot(freq,Ptx_Line, label = 'Spectral Line Tx power')
plt.grid(True,'both')
plt.title('SKA Power Limits [dBm] transmitted by a culprit at r2 distance')
plt.legend()



Etx_C = Ptx_Continuum + 10*np.log10(120*np.pi/(4*np.pi*r1**2)) - 30 + 120 - 10*np.log10(N)# -30 to get to dBW +120 to get to dBuV/m,
Etx_L = Ptx_Line + 10*np.log10(120*np.pi/(4*np.pi*r1**2)) - 30 + 120 - 10*np.log10(N)# -30 to get to dBW +120 to get to dBuV/m,

plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,Etx_C, label = 'Continuum')
L2, = plt.plot(freq,Etx_L, label = 'Line')
#L3, = plt.plot(freq,Etx_menor-shield+shield_MK, label = 'Allowed Tx Electric field at r1 distance (MEERKAT DISH)')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBuV/m]")
plt.xscale('log')
plt.grid(True,'both')
plt.title('Allowed emissions levels measured with an antenna at 1m distance, SKA RBWs, victim culprit distance = 1m')
plt.legend()




#%% Scale by the RBW used for measurements
# Because the emissions measurements are made with Spectrum analyzers, or limit lines in standards, the RBW are not the same as the SKA ones.
# The RBW to make the measurements is different, so the question is how to scale the amplitudes.
# considering that the worst case is when the emissions are narrow band, if the RBW of the receiver is narrower than the Tx 
# measurement system, the RX power would be the same, i.e. no scaling by the RBW. In the case the RBW of the receiver is greater
# then the transmited power sould be scaled by the ratio of the RBWs = RBW_lower/RBW_greater, i.e the culprit should emit less
# power

# Continuum
RBW1 = RBW_meas
RBW2 = RBW_Continuum
RBW_scale = np.ones(np.size(RBW1))
for i,val in enumerate(RBW_Continuum):
    if RBW1[i] < RBW2[i]:
        RBW_scale[i] = (RBW1[i]/RBW2[i])
Ptx_Continuum_scaled = Ptx_Continuum + 10*np.log10(RBW_scale)


#Spectral line
RBW1 = RBW_meas
RBW2 = RBW_Line
RBW_scale = np.ones(np.size(RBW1))
for i,val in enumerate(RBW_Line):
    if RBW1[i] < RBW2[i]:
        RBW_scale[i] = (RBW1[i]/RBW2[i]) 
Ptx_Line_scaled = Ptx_Line + 10*np.log10(RBW_scale)


# find the lowest value of permitted emissions 
Ptx_menor = np.minimum(Ptx_Continuum_scaled,Ptx_Line_scaled)



plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,Ptx_Continuum_scaled, label = 'Continuum Tx power')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBm]")
plt.xscale('log')
L2, = plt.plot(freq,Ptx_Line_scaled, label = 'Spectral Line Tx power')
L3, = plt.plot(freq,Ptx_menor, label = 'Minimum permited Tx power')
plt.grid(True,'both')
plt.title('SKA Power Limits [dBm] transmitted by a culprit at  r2 distance considering shielding, scaled by the difference in RX RBW and measure RBW')
plt.legend(handles = [L1,L2,L3])


#%% Electric field measured by an antenna at X meters from the culprit
# calculates the electric field that the permited Tx power generates at a dostance X from the culprit,
# this plot is intended to be able to compare with the CISPR 22 emissions limit (or other limits)


Etx_menor = Ptx_menor + 10*np.log10(120*np.pi/(4*np.pi*r1**2)) - 30 + 120 - 10*np.log10(N)# -30 to get to dBW +120 to get to dBuV/m,


plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,Etx_menor, label = 'Allowed Tx Electric field at r1 distance')
L2, = plt.plot(freq,CISPR22_i, label = Std_name)
#L3, = plt.plot(freq,Etx_menor-shield+shield_MK, label = 'Allowed Tx Electric field at r1 distance (MEERKAT DISH)')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBuV/m]")
plt.xscale('log')
plt.grid(True,'both')
plt.title('Allowed emissions levels measured with an antenna at CISPR distance, considering the culprit at 100m from the victim')
plt.legend()


Margin = CISPR22_i - Etx_menor
plt.figure(figsize=(20, 13), dpi=80, facecolor='w', edgecolor='k')
L4, = plt.plot(freq,Margin+20*np.log10(r2/1), label = '1 m', linewidth =3)
L2, = plt.plot(freq,Margin+20*np.log10(r2/10), label = '10 m', linewidth =3)
L1, = plt.plot(freq,Margin, label = str(r2) + ' m', linewidth =3)
L3, = plt.plot(freq,Margin+20*np.log10(r2/1000), label = '1 km', linewidth =3)
L5, = plt.plot(freq,Margin+20*np.log10(r2/10000), label = '10 km', linewidth =3)
plt.stem([50,350,20000],[150,150,150],'r')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dB]")
plt.xscale('log')
plt.grid(True,'both')
plt.title('Attenuation needed for a ' + Std_name +' culprit to comply with SKA limits')
plt.legend(bbox_to_anchor=(1, 0.9), bbox_transform=plt.gcf().transFigure)
#plt.legend(handles = [L1,L2,L3,L4,L5],pos=[0,0])







