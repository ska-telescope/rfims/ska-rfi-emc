# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 11:06:26 2019

@author: f.divruno
"""

import numpy as np

Tx_gain = (-6,-8.5,-10,-11,-11,-11,-11)

fo = 40
for i in range(len(Tx_gain)):
    Dist = 10**((64 + Tx_gain[i] - 20*np.log10((i+2)*fo) + 27.55)/20)
    print('Fo=%d, harmonic nr %d = %d , Dist= %.0f ' % (fo,i+2,(i+2)*fo,Dist))


fo = 80
for i in range(len(Tx_gain)):
    Dist = 10**((64 + Tx_gain[i] - 20*np.log10((i+2)*fo) + 27.55)/20)
    print('Fo=%d, harmonic nr %d = %d , Dist= %.0f ' % (fo,i+2,(i+2)*fo,Dist))


fo = 150
for i in range(len(Tx_gain)):
    Dist = 10**((64 + Tx_gain[i] - 20*np.log10((i+2)*fo) + 27.55)/20)
    print('Fo=%d, harmonic nr %d = %d , Dist= %.0f ' % (fo,i+2,(i+2)*fo,Dist))
    