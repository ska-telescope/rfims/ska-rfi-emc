# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 16:04:36 2019

@author: f.divruno
"""
import numpy as np
import matplotlib.pyplot as plt
from EMC_library import stds

freqs = np.linspace(100,2000,20)




plt.figure()
Emi= stds(freqs[0],freqs[-1],20,10,'EN62040C3')
plt.plot(freqs,Emi.EIRP)
plt.plot(freqs,Emi.EIRP,label='EN6240C3')
Emi= stds(freqs[0],freqs[-1],20,10,'EN61000_6_4')
plt.plot(freqs,Emi.EIRP,label='EN61000_6_4')
Emi= stds(freqs[0],freqs[-1],20,10,'CISPR22A')
plt.plot(freqs,Emi.EIRP,label='CISPR22A')

plt.title('EIRP of equipment complying to commercial standards')
plt.grid('on')
plt.legend()
plt.xlabel('MHz')
plt.ylabel('dBm')

