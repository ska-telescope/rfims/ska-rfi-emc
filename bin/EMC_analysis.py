# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 15:02:33 2019




@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from pycraf import pathprof
import pandas as pd
#from EMC_library import stds
from radEmission import radEmission
from EMC_analysis_shielding import EMC_analysis_shielding
from rfiLib.SKA_EMIEMC_std import SKA_EMIEMC_std
from pycraf import conversions as cnv
import matplotlib

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 18}	
matplotlib.rc('font', **font)


pathprof.SrtmConf.set(download='missing',
                      server='viewpano', #server='nasa_v2.1',
                      srtm_dir=r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\STRM_data')

resultDir = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\EMC_analysis'



#%%
def radiatedEmissions(stdName,N,freq):
    
    Emi = radEmission(stdName,N,freq)
    return Emi



def propagationLoss(X,skaAntPos,freqs):
    '''
    Calculates the propagation loss from the point X to the points in skaAntPos
    input:
        X: [longitude, latitude]
        skaAntPos: [longitude, latitude]
        freq
    '''
    temperature = 290*u.K
    pressure = 1023*u.hPa
    h_tg = 2*u.m
    h_rg = 2*u.m
    timepercent = 2*u.percent
    hprof_step=50*u.m


    lon = np.array(skaAntPos['lon'])*u.deg
    lat = np.array(skaAntPos['lat'])*u.deg

    if (X[0]==0*u.deg) or (X[1] == 0*u.deg): #in the case of dish PSC esquiment
        att = 20*np.log10(10)+20*np.log10(freqs.value) - 27.55
        propLossAnt,blk = np.meshgrid(att,np.ones(len(skaAntPos)))
        print('Calculating FSPL for 10 m distance')
    else:
        
        N_ants = len(lon)
        N_freqs = len(freqs)    
        lon_tx = X[0]
        lat_tx = X[1]
        
        try:
            Aux = np.load(resultDir+'\\'+telescope+'_'+location+'_'+str(hprof_step)+'.npz')
            propLossAnt = Aux['propLossAnt']
            print('Loading attenuation paths from  '+location+ ' to each antenna')
            if np.size(propLossAnt,1) != N_freqs:
                 raise Exception()
                
        except:
            
            propLossAnt = np.zeros([N_ants,N_freqs])
            
            print('Calculating attenuation paths from  '+location+ ' to each antenna')
            
            results=[]
            hprof_cache =[]
        
            # Consider the attenuation of ther berm
            if ('Site Complex' in location) and (telescope=='MID'):
                Berm_att =  np.where((skaAntPos.lat>=-30.75) & (skaAntPos.lon>=21.43),15,0)
            else:
                Berm_att = np.zeros(N_ants)
        
            for k in range(N_ants): # for each antenna
                hprof_cache = pathprof.height_path_data(lon_tx, lat_tx, lon[k], lat[k], hprof_step)
                
                for i in range(N_freqs): #for each frequency point
                    results = pathprof.atten_path_fast(freqs[i], temperature, pressure, h_tg, h_rg, timepercent,hprof_cache,)
                    propLossAnt[k,i] = results['L_b'][-1].value + Berm_att[k]
    
                    if results['L_b'][-1].value < 1:
                        # calculate atten with FSPL:
                        att = 20*np.log10(hprof_cache['distances'][-1]*1e3)+20*np.log10(freqs[i].value) - 27.55
                        propLossAnt[k,i] = att
    
                    print(location,' to ',skaAntPos.name[k],' freq %.0f atten value %.2f'%(freqs[i].value,propLossAnt[k,i]))
            
            np.savez(resultDir+'\\'+telescope+'_'+location+'_'+str(hprof_step),propLossAnt=propLossAnt)       

    return propLossAnt

def calcRadiatedPower(culprits,freqs):
    '''
     Calculates the radiated power for each of the culprits
    '''
    N_freqs = len(freqs)
    P = np.zeros(N_freqs)
    radPowerList = list()
    
    for i in culprits.index:
        cul = culprits.loc[i,:].copy() # copy the row i
        # mltiplicity:
        if cul['indoor']: 
            cul['num'] = cul['num']**0.5

        # Shielding effectiveness
        SE = EMC_analysis_shielding(cul['shielding'],freqs.value)
        
      # Radiated Emissions:        
        Emi = radiatedEmissions(cul['std'],cul['num'],freqs.value)
        
        print(cul['group'])
        print('EIRP:')
        print('%s'%str(np.round(Emi.EIRP,decimals=2)))
        print('Shielding Effectiveness:')
        print('%s'%str(np.round(SE,decimals=2)))
        
        # Total radiated power:
        radPower = Emi.EIRP - SE 
        radPowerList.append(list([cul['location'],cul['group'],radPower]))
        # Calculate the total power radiated from the location
        P+= 10**(radPower/10)

    P = 10*np.log10(P)
    N_culprits = len(culprits)
    plt.figure()
    for i in range(N_culprits):
        plt.semilogx(freqs,radPowerList[i][2],label=radPowerList[i][1],linewidth=3)
    plt.legend()
    plt.ylabel('dBm')
    plt.xlabel('MHz')
    plt.grid()
    plt.title('Radiated power from Location: %s, all groups'%(location))
        
    
    plt.figure()
    plt.semilogx(freqs,P)
    plt.title('Total radiated power from the %s' % location)
    plt.ylabel('dBm')
    plt.xlabel('MHz')
    plt.grid()
    return radPowerList,P

def calcReceivedPower(powerRad,propLossAnt, freqs, groupName,plotFlag=0):
    SKA_std = SKA_EMIEMC_std(freqs.value,0)
    
    powerRadMesh,blk = np.meshgrid(powerRad,np.ones(len(propLossAnt)))
    
    powRxAnt = powerRadMesh - propLossAnt
    
    powThCont = SKA_std['SKA_Continuum_PSD'] + 10*np.log10(SKA_std['RBW_Continuum'])
    powThLine = SKA_std['SKA_Line_PSD'] + 10*np.log10(SKA_std['RBW_Line'])
    
    # correction for bandwidth of the measurement system 
    RBW = np.interp(freqs,[0,1000,1000,20000],[120e3,120e3,1e6,1e6])
    powThCont =  powThCont - 10*np.log10(SKA_std['RBW_Continuum']/RBW) 
    
    if plotFlag:
        plt.figure(figsize=(10, 10))
        plt.semilogx(freqs,np.transpose(powRxAnt))
        plt.semilogx(freqs,powThLine,'b',linewidth=3,label ='Spectral Line threshold')
        plt.semilogx(freqs,powThCont,'r',linewidth=3,label ='Continuum threshold')
        plt.ylabel('dBm')
        plt.xlabel('MHz')
        plt.grid()
        plt.legend()
        plt.title('power received from group: '+groupName+ ' in location: '+location)
    return powRxAnt,powThCont,powThLine


def calcMargins(powRxAnt,groupName,plotFlag=0):
    # A negative margin means non compliance.    
    
    margCont = np.min(powThCont - powRxAnt )
    margLine = np.min(powThLine - powRxAnt )
    
    Margin = min(margCont,margLine)
    
    if Margin<=0: #there is a non compliance
        aux = np.where(powRxAnt>=powThCont) 
        NC1 = np.unique(aux[0])
        
        aux = np.where(powRxAnt>=powThLine) 
        NC2 = np.unique(aux[0])
    
        NCindex = np.unique(np.concatenate((NC1,NC2)))
        print('\n\nNon Compliant antennas are:\n',skaAntPos.name[NCindex])
        
    else: # no non compliances
        NCindex = []
        print('\n\nGroup and location compliant.\nMin Margin: ',Margin)
            
    # Compute the margin and the distribution.
    # A negative margin means non compliance. 
    margCont = (powThCont - powRxAnt )
    margLine = (powThLine - powRxAnt )   

    nCont,binsCont = np.histogram(margCont.flatten(),100)
    nLine,binsLine = np.histogram(margLine.flatten(),100)

    if plotFlag:
        plt.figure()
        plt.bar(binsCont[0:-1],nCont,width=2,label = 'Continuum margin')
        plt.bar(binsLine[0:-1],nLine,width=1,label = 'Spectral Line margin')
        plt.title('Histogram of the margins from group: '+groupName+ ' in location: '+location)
        plt.plot([0,0],[0,max(np.concatenate((nLine,nCont)))],'r',linewidth=3)
        plt.legend()
        plt.xlabel('Margin [dB]')
        plt.ylabel('Occurrences')
    #average margin in each antenna:
    marginMin = np.min(np.concatenate((margLine,margCont),1),1)
    hist = list([binsCont,nCont,binsLine,nLine])
    return marginMin,NCindex,hist

def calcMetrics(powRxAnt,groupName,TUM,REML,SEML):
    
    margCont = (powThCont - powRxAnt )
    margLine = (powThLine - powRxAnt )
    marg = np.min(np.array([margCont,margLine]),0) # get the minimum margin forr each freq point
    
    
    ncMargin = marg[marg<=0]
    if ncMargin.size > 0:
        meanNcMargin = np.average(ncMargin)
        stdNcMargin = np.std(ncMargin)
        minNcMargin = np.min(ncMargin)
    else:
        meanNcMargin = np.nan
        minNcMargin = np.nan
        
    # margin to the core
    if victim == 'MID':
        coreIndex = [25, 24, 30, 31, 34, 35, 36, 37, 39, 40, 41, 47, 43 ]   # index in the CSV where the antennas are.
    if victim == 'LOW':
        coreIndex = [216, 193,  200,  203,  218,  152, 195, 207, 174, 198 ]   # index in the CSV where the stations are.
    if victim == 'ASKAP-MWA':
        coreIndex = [1]
        
#                    [C217 , C192, C201, C204,  C219, C153, C196, C208, C175, C199]
    coreMinMarg = np.min(marg[coreIndex])
    
    #-------Min Margin Metric
    # goes from 0 to 3
    MMM_legend = list((
                '0 (Min Margin > 0)',
                '1 (-3 < Min Margin <= 0)',
                '2 (-10 < Min Margin <= -3)',
                '3 (Min Margin <= -10)'
                ))
    if ncMargin.size > 0:
        if (minNcMargin ==0)  : MMM = 0
        if (minNcMargin < 0) & (minNcMargin >= -3): MMM = 1
        if (minNcMargin < -3) & (minNcMargin >= -10): MMM = 2
        if (minNcMargin < -10) : MMM = 3
    else:
        MMM = 0
        
    #--------Afected Antennas Metric
    # goes from 0 to 100
    AAM = len(NCindex)/len(skaAntPos)*100
    
    # Time usage Metric
    # goes from 0 to 3
    TUM_legend = list((
                 '0 (very ocasional operation)',
                 '1 (short time operation)',
                 '2 (long time operation)',
                 '3 (continuous use)'
                ))
#    TUM = 2
    
    
    #--------- Total Impact Metric
    # goes from 0 to 100
    TIM = AAM*MMM*TUM/(3*3)
    
    
    print('\nLocation "%s" - Group %s metrics:  \
            \nAffected Antennas: %.2f%%\
            \nMin Margin Metric: %s\
            \nTime Occupancy Metric: %s\
            \nTotal Impact Metric: %.2f%%\
            '%(location, groupName, AAM, MMM_legend[MMM],TUM_legend[TUM],TIM))
    
    print('\nMargins:  \
            \nMin Margin in the Core: %.2f\
            \nMin Non Compliant Margin: %.2f\
            \nMean Non Compliant Margin: %.2f\
            '%(coreMinMarg,minNcMargin,meanNcMargin))
    
    
    # Maturity level of Emissions and shielding
    # Emissions ML
    # goes from 0 to 3
    REML_legend = list((
                 '1 (Design - CoC)',
                 '2 (Design + frequencies analysis / previous experience)',
                 '3 (Prototype measurements)'
                ))
#    REML = 1
    
    # Shielding ML
    # goes from 0 to 30
    
    SEML_legend = list((
                 '1 (Design spec)1',
                 '2 (Design spec + previous experience)',
                 '3 (Measurements)'
                ))
#    SHML = 2
    
    # Total Maturity level
    TML = (REML+SEML)-1
    
    
    print('\nTotal Metrics:  \
            \nTotal Impact Metric (TIM): %d%%\
            \nTotal Maturity level (TML): %d\
            '%(TIM,TML))
    

def simControl(N_freqs):
    '''
     Flow control of the simulation
     input:
         N_freqs: number of frequency points
    '''

    nTelescope = int(input('\n\nSelect \nMId:0 \nLOW:1 \nASKAP-MWA:2 \nSelection:'))
    if nTelescope == 1:
        telescope = 'LOW'
    elif nTelescope == 0:
        telescope = 'MID'
    elif nTelescope == 2:
        telescope = 'ASKAP-MWA'
    else:
        raise Exception('No such telescope - exit')
    
    if telescope == 'MID':
        victim = 'MID'
        fmin = 350
        fmax = 15400
        culpritsCsvFile = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\SKA1_Mid_EMC_analysis_locations.csv'
        skaAntPosCsvFile = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\SKA1_Mid_coordinates.csv'    
        
    elif telescope == 'LOW':
        victim = 'LOW'
        fmin = 50
        fmax = 350
        culpritsCsvFile = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\SKA1_Low_EMC_analysis_locations.csv'
        skaAntPosCsvFile = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\SKA1_Low_coordinates.csv'    
    
    elif telescope == 'ASKAP-MWA':
        victim = 'ASKAP-MWA'
        telescope = 'LOW'
        fmin = 100
        fmax = 2500
        culpritsCsvFile = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\SKA1_Low_EMC_analysis_locations.csv'
        skaAntPosCsvFile = r'C:\Users\F.Divruno\Dropbox (SKA)\Python_codes\MRO_telescopes.csv'    

    
    skaAntPos = pd.read_csv(skaAntPosCsvFile,comment='#')
    
    freqs = np.array([50,70,100,150,230,300,350,400,500,990,1010,1100,1800,2000,15.4e3,20e3])
#    freqs = np.logspace(np.log10(fmin),np.log10(fmax),N_freqs)*u.MHz    
    freqs = freqs[(freqs>=fmin)&(freqs<=fmax)]*u.MHz
    
    aux = pd.read_csv(culpritsCsvFile,comment='#')
    print('\n\n\n\n')
    print(aux.location)
    loc_num = int(input('\n\nSelect location to analyze: '))
    location = aux.location[loc_num]
    print(aux.group[aux.location==location])
    group_num = input('\n\nSelect group to analyze (type "all" for all groups): ')
    
    
    if group_num == 'all':
        culprits = aux[(aux.telescope==telescope) & (aux.location==location)] #only one location
        group = 'all'
    else:
        group_num = int(group_num)
        group = aux.group[group_num]    
        culprits = aux[(aux.telescope==telescope) & (aux.location==location) & (aux.group==aux.group[group_num])] # Only one group in the location


    N_culprits = culprits.count()[0]
    N_locations = len(culprits.location.unique())


    return victim,telescope,skaAntPos,location,group,culprits,freqs
    

#%% Cimulation control
'''
===========================================
    Sim control
===========================================
'''
victim,telescope,skaAntPos,location,group,culprits,freqs = simControl(N_freqs=5)

#%%
'''
===========================================
    Find the attenuation at each position of SKA antennas using path info
===========================================
'''
lon_tx = np.array(culprits.loc[culprits.location==location,'lon'])[0]*u.deg
lat_tx = np.array(culprits.loc[culprits.location==location,'lat'])[0]*u.deg

propLossAnt = propagationLoss([lon_tx, lat_tx],skaAntPos,freqs)

#%% 
'''
===========================================
    Calculate the radiated power of each group in the location analysed
===========================================
'''
radPowerList,P = calcRadiatedPower(culprits,freqs)

#%%
'''
===========================================
    Plot the received power considering 0dBi antenna gain
===========================================
'''
powerRad = P # total power radiated by the location
powRxAnt,powThCont,powThLine = calcReceivedPower(powerRad,propLossAnt, freqs, groupName='all',plotFlag=1)
blk1,blk2,hist = calcMargins(powRxAnt,groupName='all',plotFlag=1)

#%%
'''
============================================================
    Find the antennas that receive more power than the limits
    plot the historams of the power margins
============================================================
'''

marginMin,NCindex,hist = calcMargins(powRxAnt,groupName='all')

#fig1 = plt.figure() # figure for received power from each location
#fig2 = plt.figure() # figure for histogram from each location
plt.figure('fig1', figsize=(15,10))
plt.figure('fig2', figsize=(15,10))    
for i in range(len(radPowerList)):
    #Rx power
    powRxAnt,powThCont,powThLine = calcReceivedPower(radPowerList[i][2],propLossAnt, freqs, groupName=radPowerList[i][1])
    plt.figure('fig1')
    plt.gcf()
#    plt.subplot(int(len(radPowerList)/2)+1,int(len(radPowerList)/2)+1,i+1)    
    plt.subplot(3,3,i+1)
    plt.semilogx(freqs,np.transpose(powRxAnt))
    plt.semilogx(freqs,powThLine,'b',linewidth=3,label ='Spectral Line threshold')
    plt.semilogx(freqs,powThCont,'r',linewidth=3,label ='Continuum threshold')
    plt.ylabel('dBm')
    plt.xlabel('MHz')
    plt.grid()
    plt.legend()
    plt.title(radPowerList[i][1])
    
    #Margins histogram
    blk1,blk2,hist = calcMargins(powRxAnt,groupName=radPowerList[i][1])
    plt.figure('fig2')
    plt.gcf()
#    plt.subplot(int(len(radPowerList)/2)+1,int(len(radPowerList)/2)+1,i+1)    
    plt.subplot(3,3,i+1)
    plt.bar(hist[0][0:-1],hist[1],width=2,label = 'Continuum margin')
    plt.bar(hist[2][0:-1],hist[3],width=1,label = 'Spectral Line margin')
    plt.plot([0,0],[0,max(np.concatenate((hist[1],hist[3])))],'r',linewidth=3)
    plt.legend()
    plt.xlabel('Margin [dB]')
    plt.ylabel('Occurrences')
    plt.grid()
    plt.title(radPowerList[i][1])
    
plt.figure('fig1')
plt.suptitle('Received power from location ' + location)
#plt.subplots_adjust(hspace = 0.3)
#plt.tight_layout()
plt.figure('fig2')
plt.suptitle('Histogram of the margins from location '+location)
plt.subplots_adjust(hspace = 0.3)
#%% Calculate the residual risk
'''
============================================================
    Calculate the metrics
============================================================
'''

for i in range(len(radPowerList)):
    groupName = radPowerList[i][1]
    TUM = np.array(culprits[culprits['group']==groupName]['TU'])[0]
    REML = np.array(culprits[culprits['group']==groupName]['REML'])[0]
    SEML = np.array(culprits[culprits['group']==groupName]['SEML'])[0]
    powRxAnt,powThCont,powThLine = calcReceivedPower(radPowerList[i][2],propLossAnt, freqs, groupName=radPowerList[i][1])
    calcMetrics(powRxAnt,groupName,TUM,REML,SEML)
    
#%%
'''
============================================================
   Draw antennas and culprit on the map 
   and also draw the some contours in attenuation map
============================================================
'''

map_size_lon, map_size_lat = 2 * u.deg, 2 * u.deg
map_resolution = 0.001* u.deg #0.0001 deg is possible but lenghty
hprof_step = 100 * u.m


# get the data for the map
lons, lats, heightmap = pathprof.srtm_height_map(
    lon_tx, lat_tx,
    map_size_lon, map_size_lat,
    map_resolution=map_resolution,
    hprof_step = hprof_step
    )


# Plot the map
_lons = lons.to(u.deg).value
_lats = lats.to(u.deg).value
_heightmap = heightmap.to(u.m).value

fig = plt.figure(figsize=(15, 10))
ax = fig.add_axes((0.1, 0.1, 0.8, 0.8))

vmin,vmax = np.min(_heightmap),np.max(_heightmap) 

terrain_cmap,terrain_norm = pathprof.terrain_cmap_factory(sealevel=vmax/10,vmax=vmax)

cim = ax.imshow(
                _heightmap,
                origin='lower', interpolation='nearest',
                cmap=terrain_cmap, norm=terrain_norm,
                vmin=vmin, vmax=vmax,
                extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
                )
ax.set_xlabel('Longitude [deg]')
ax.set_ylabel('Latitude [deg]')

# set aspect ratio and limits of the image
ax.set_aspect(abs(_lons[-1] - _lons[0]) / abs(_lats[-1] - _lats[0])) 
ax.set_ylim([_lats[0],_lats[-1]])
ax.set_xlim([_lons[0],_lons[-1]])
plt.title('SKA configuration - group: '+group+' in location: '+location )
#ax.set_aspect(1)

# Place a marker in each of the SKA antennas in the map
marginMin[marginMin>40] = 40 #clipping to 40 db of margin
cim = ax.scatter(skaAntPos.lon, skaAntPos.lat,c= marginMin,linewidth=2, cmap='jet_r') # c='g',linewidth=3)
#for i in range(N_ants):
#    ax.scatter(skaAntPos.lon[i], skaAntPos.lat[i],marker='o', c='g',linewidth=3)
    

#for i in range(len(AntNonCompliant)):
#ax.scatter(skaAntPos.lon[NCindex], skaAntPos.lat[NCindex],marker='o', s=120,facecolors='none', edgecolors='white',linewidth=0.5)
#ax.scatter(skaAntPos.lon[NCindex], skaAntPos.lat[NCindex],marker='x', s=100, facecolors='white',linewidth=0.5)

# Place a marker where the analyzed site is    
ax.scatter(lon_tx, lat_tx,marker='x', c='k',linewidth=3)

cbarAx = fig.add_axes((0.75, 0.1, 0.02, 0.8))

cbar = fig.colorbar(
                cim, cax=cbarAx, orientation='vertical',
                )


cbarAx.set_ylabel('Margin [dB]')
#cbarAx.set_aspect(1)

'''
# Plot the contour maps of different attenuations

ax.contour(_total_atten.to(cnv.dB).value, levels=[100],
           colors=['green'], linestyles='-',
           origin='lower',
           extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
           alpha=1)

ax.contour(_fspl_atten.to(cnv.dB).value, levels=[100],
           colors=['red'], linestyles='-',
           origin='lower',
           extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
           alpha=1)
'''
#%%
'''
=====================================
    Calculate the propagation loss map
    for some freqs0
=====================================
'''
plot_att_map=1
if plot_att_map:

    # Here input the patameters for the ITU-R 452-16 model
    map_size_lon, map_size_lat = 2 * u.deg, 2 * u.deg
    map_resolution = 0.01* u.deg #0.0001 deg is possible but lenghty
    hprof_step = 100 * u.m
    
    freq = 0.350 * u.GHz
    omega = 0. * u.percent  # fraction of path over sea
    temperature = 290. * u.K
    pressure = 1013. * u.hPa
    timepercent = 0.1 * u.percent  # see P.452 for explanation
    h_tg, h_rg = 2 * u.m, 10 * u.m #height of the receiver and transmitter above gnd
    G_t, G_r = 0 * cnv.dBi, 0 * cnv.dBi
    zone_t, zone_r = pathprof.CLUTTER.UNKNOWN, pathprof.CLUTTER.UNKNOWN
    
    
    ## Fasster approach
    
    hprof_cache = pathprof.height_map_data(lon_tx, lat_tx,
                                            map_size_lon, map_size_lat,
                                            map_resolution=map_resolution,
                                            zone_t=zone_t, zone_r=zone_r,
                                            )
    
    results = pathprof.atten_map_fast(freq,
                                    temperature,
                                    pressure,
                                    h_tg, h_rg,
                                    timepercent,
                                    hprof_cache,  # dict_like
                                    )
    
    
    #_lons = hprof_cache['xcoords']
    #_lats = hprof_cache['ycoords']
    _total_atten = results['L_b']  # L_b is the total attenuation, considering all the factors.
#    _fspl_atten = results['L_bfsg']  # considers only the free space loss
    
    
    # Plot the results selected
    vmin, vmax = 100, 200 # Max and min scale
#%%
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_axes((0.1, 0.15, 0.8, 0.8))
    cbax = fig.add_axes((0.25, 0, 0.5, .02))
    _lons = hprof_cache['xcoords']
    _lats = hprof_cache['ycoords']
    cim = ax.imshow(
        _total_atten.to(cnv.dB).value,
        origin='lower', interpolation='nearest', cmap='inferno_r',
        vmin=vmin, vmax=vmax,
        extent=(_lons[0], _lons[-1], _lats[0], _lats[-1]),
        )
    cbar = fig.colorbar(
        cim, cax=cbax, orientation='horizontal'
        )
    #ax.set_aspect(abs(_lons[-1] - _lons[0]) / abs(_lats[-1] - _lats[0]))
    #cbar.set_label(r'Path propagation loss')
    cbax.xaxis.set_label_position('top')
    for t in cbax.xaxis.get_major_ticks():
        t.tick1line.set_visible(True)
        t.tick2line.set_visible(True)
        t.label1.set_visible(True)
        t.label2.set_visible(True)
    ctics = np.arange(vmin, vmax, 10)
    cbar.set_ticks(ctics)
    cbar.ax.set_xticklabels(map('{:.0f} dB'.format, ctics))
    ax.set_xlabel('Longitude [deg]')
    ax.set_ylabel('Latitude [deg]')
    ax.set_autoscale_on(False)
    
    #Place a marker in each antenna location.
    ax.scatter(skaAntPos.lon, skaAntPos.lat,c= 'w',linewidth=2)
    # Place a marker where the analyzed site is    
    ax.scatter(lon_tx, lat_tx,marker='x', c='k',linewidth=3)
