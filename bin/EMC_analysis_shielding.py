# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 11:28:07 2019

@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt
import numbers


def cpfShielding(freq):
    # Shielding outside the DRA in the MID CPF
    SE = 0.0000254*freq**2-0.00755*freq+22.56
    SE[freq>1000] = SE[freq<=1000][-1]
    return SE


def pscShielding(freq):
    SE = np.where(freq<13.8e3,80,50)
    return SE

def emiScShielding(freq):
    SE = np.where(freq<13.8e3,80,60)
    return SE

def emiDcShielding(freq):
    SE = np.ones(len(freq))*60
    return SE

def dryCoolersShielding(freq):
    f = [100, 500, 1000, 2500, 8000]
    SE = [100, 70, 60, 40, 20]
    SE = np.interp(freq,f,SE)
    return SE


def EMC_analysis_shielding(SE,f):
    '''
        Shielding of different equipment groups
        name: name of the shielding enclosure or value
        N: multiplier in linear units(this will multiply the radiated emissions)
        freq: frequency vector in MHz
        
        
    '''
    freq = np.logspace(0,5,10000)
    try:
        
        if isinstance(float(SE),numbers.Number):
            SE = float(SE)*np.ones(len(freq))
        
    except:
        if SE == 'SE dry-cooler':
            SE = dryCoolersShielding(freq)
            
        elif SE == 'MID CPF':
            SE = cpfShielding(freq)
            
        elif SE == 'DISH EMIDC':
            SE = emiDcShielding(freq)
            SE += pscShielding(freq)
            
        elif SE == 'DISH EMISC':
            SE = emiScShielding(freq) 
            SE += pscShielding(freq)
    
    return np.interp(f,freq,SE)




#%% TEST
    
import matplotlib
font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
matplotlib.rc('font', **font)

if __name__=='__main__':
    freq = np.array([50,100,220,235,990,1100,2990,3050,20000])
#    freq = np.linspace(10,20e3,1000)
    SEs = ['100','SE dry-cooler','MID CPF','DISH EMIDC','DISH EMISC']
    plt.figure()
    i=len(SEs)
    for name in SEs:
        SE = EMC_analysis_shielding(name,freq)
        plt.semilogx(freq,SE, label = name ,linewidth=i)
        i-=0.6
    
    plt.title('Shielding for EMC analysis')
    plt.xlabel('Mhz')
    plt.ylabel('dB')
    plt.legend()
    
