# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 12:03:11 2018

EMC Budget MID

@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt
from math import * 
import matplotlib
import EMC_library as EMC
from rfiLib.SKA_EMIEMC_std import SKA_EMIEMC_std

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
matplotlib.rc('font', **font)



#%% Define the frequency vector in MHz
Npoints = 50 # Number of points to represent the frequency
fmin = 30
fmax = 25000
freq = np.logspace(np.log10(fmin),np.log10(fmax),Npoints) #[MHz]
#freq = np.linspace(fmin,fmax,Npoints) #[MHz]
K_N_culprits = 5 # this is equivalent to Ptot = Sqrt(N)*Ptot


#%% Import standards
R_meas = 10
stds = EMC.stds(fmin,fmax,Npoints,R_meas)

SKA_std = SKA_EMIEMC_std(fmin,fmax,Npoints,1)
freq  = SKA_std['freq']
SKA_Continuum_PSD = SKA_std['SKA_Continuum_PSD']
RBW_Continuum  = SKA_std['RBW_Continuum']
SKA_Continuum_dBm  = SKA_std['SKA_Continuum_dBm']
SKA_Line_PSD  = SKA_std['SKA_Line_PSD']
RBW_Line  = SKA_std['RBW_Line']
SKA_Line_dBm  = SKA_std['SKA_Line_dBm']

#%% Location  
Location = 'Site complex'

# distance from victim (Dish or station) to culprit. in metre
r2 = [2500,2500,2500,2500]
r2_f = [fmin,300, 300, fmax] #Frequency of the different distance values
r2 = np.interp(freq,r2_f,r2)
#r2 = 6000 # Uncomment this for fixed distance for all frequencies

#%%Culprit #1 : DRA area
culprit = 'DRA'
# Shielding of the building / enclosure [dB]
shield = [80,80,80,80]
shield_f = [fmin, 13.8e3,13.8e3,fmax]
shield = np.interp(freq,shield_f,shield)
#shield = 0*np.ones(Npoints) # Uncomment for fixed shielding for all frequencies

extra_shield = 0 #shield from KAPB walls or racks

shield = shield + extra_shield


N_culprits = 2200

#EIRP = E^2/Eta*4PI*R
        #dBV/m a dBuV/m = +120
        # dBW a dBm = +30
        

EIRP = stds.CISPR22A_E + 10*np.log10(4*np.pi*R_meas**2/120/np.pi) - 120 +30 

EIRP = EIRP + K_N_culprits*np.log10(N_culprits)

# BW correction:
EIRP_Continuum = EIRP + 10*np.log10(RBW_Continuum/stds.CISPR22A_RBW)
EIRP_Line = EIRP

extra_FSPL_att = 10
FSPL = 20*np.log10(freq) + 20*np.log10(r2) - 27.56 + extra_FSPL_att

Prx_1_Cont =  EIRP_Continuum - FSPL - shield
Prx_1_Line =  EIRP_Line - FSPL - shield



plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,Prx_1_Cont, label = 'Continuum case Rx power')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBm]")
plt.xscale('log')
L2, = plt.plot(freq,Prx_1_Line, label = 'Spectral Line Rx power')
L3, = plt.plot(freq,SKA_Continuum_dBm, label = 'SKA Continuum')
L4, = plt.plot(freq,SKA_Line_dBm, label = 'SKA Spectral Line')
plt.grid(True,'both')
plt.title('SKA Power Limits [dBm] vs received power from '+Location+' - '+culprit)
plt.legend(handles = [L1,L2,L3,L4])


#%%Culprit #2 : KAPB area
culprit = 'KAPB'
# Shielding of the building / enclosure [dB]
shield = [20,20,20,20]
shield_f = [fmin, 13.8e3,13.8e3,fmax]
shield = np.interp(freq,shield_f,shield)
shield = 30*np.ones(Npoints) # Uncomment for fixed shielding for all frequencies

extra_shield = 0 #shield from KAPB walls

shield = shield + extra_shield


N_culprits = 20

#EIRP = E^2/Eta*4PI*R
        #dBV/m a dBuV/m = +120
        # dBW a dBm = +30
        

EIRP = stds.CISPR22A_E + 10*np.log10(4*np.pi*R_meas**2/120/np.pi) - 120 +30 

EIRP = EIRP + 5*np.log10(N_culprits)

# BW correction:
EIRP_Continuum = EIRP + 10*np.log10(RBW_Continuum/stds.CISPR22A_RBW)
EIRP_Line = EIRP

extra_FSPL_att = 10 #+ 15 #extra 15 for the berm
FSPL = 20*np.log10(freq) + 20*np.log10(r2) - 27.56 + extra_FSPL_att

Prx_1_Cont =  EIRP_Continuum - FSPL - shield
Prx_1_Line =  EIRP_Line - FSPL - shield



plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq,Prx_1_Cont, label = 'Continuum case Rx power')
plt.xlabel("frequency [MHz]")
plt.ylabel("[dBm]")
plt.xscale('log')
L2, = plt.plot(freq,Prx_1_Line, label = 'Spectral Line Rx power')
L3, = plt.plot(freq,SKA_Continuum_dBm, label = 'SKA Continuum')
L4, = plt.plot(freq,SKA_Line_dBm, label = 'SKA Spectral Line')
plt.grid(True,'both')
plt.title('SKA Power Limits [dBm] vs received power from '+Location+' - '+culprit)
plt.legend(handles = [L1,L2,L3,L4])

