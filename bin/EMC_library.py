# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 18:45:05 2019

@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt
from math import * 
import matplotlib

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
matplotlib.rc('font', **font)



class stds(object):
    
    def __init__(self, freqs, meas_dist=1,name=''):
        '''
            parameters:
                freq_start: 
                freq_stop:
                N: number of points
                meas_dist: 
        '''
        N = len(freqs)
        self.fdata = freqs
        self.name = name
        
        #CISPR22 Class A
        self.CISPR22A_E_f = np.array([30,230,230,1000,1000,30000,3000,6000])
#        self.CISPR22A_E = np.array([52, 52, 52, 52, 56,56,60, 60]) #dBuV/m
        self.CISPR22A_E = np.array([52, 52, 52, 52, 76,76,80, 80]) #dBuV/m        
        self.CISPR22A_RBW = [120e3, 120e3, 1e6, 1e6]
        self.CISPR22A_RBW_f = [30, 1000, 1000, 6000] #[MHz]        
        self.CISPR22A_R = [3,3, 3, 3]
        self.CISPR22A_R_f = [30,1000,1000,6000]
        self.CISPR22A_E = np.interp(self.fdata,self.CISPR22A_E_f,self.CISPR22A_E)
        self.CISPR22A_RBW = np.interp(self.fdata,self.CISPR22A_RBW_f,self.CISPR22A_RBW)
        self.CISPR22A_R = np.interp(self.fdata,self.CISPR22A_R_f,self.CISPR22A_R) 
        self.CISPR22A_E = self.CISPR22A_E + 10*np.log10(self.CISPR22A_R/meas_dist)

        #TX_MRS ( mobile radio system)
        self.TX_MRS_E_f = np.array([        40,40,80,80 ,160,160,200,200,240,240,280,280,320,320,360,360,400,400,6000])
        self.TX_MRS_EIRP = np.array([ 46,46,0 ,0  ,-40,-40,-40,-40,-40,-40,-40,-40,-40,-40,-40,-40,-40,-40,-80]) #dBuV/m
        self.TX_MRS_E = self.TX_MRS_EIRP - 5.23 + 90
        self.TX_MRS_RBW = [20e3, 20e3, 20e3, 20e3]
        self.TX_MRS_RBW_f = [30, 1000, 1000, 6000] #[MHz]        
        self.TX_MRS_R = [3,3, 3, 3]
        self.TX_MRS_R_f = [30,1000,1000,6000]
        self.TX_MRS_E = np.interp(self.fdata,self.TX_MRS_E_f,self.TX_MRS_E)
        self.TX_MRS_RBW = np.interp(self.fdata,self.TX_MRS_RBW_f,self.TX_MRS_RBW)
        self.TX_MRS_R = np.interp(self.fdata,self.TX_MRS_R_f,self.TX_MRS_R) 
        self.TX_MRS_E = self.TX_MRS_E + 10*np.log10(self.TX_MRS_R/meas_dist)

        
        #CISPR22 Class B
        self.CISPR22B_E_f = np.array([30,230,230,1000,1000,30000,3000,6000])
        self.CISPR22B_E = np.array([42, 42, 42, 42, 50,50,54, 54]) #dBuV/m
        self.CISPR22B_RBW = [120e3, 120e3, 1e6, 1e6]
        self.CISPR22B_RBW_f = [30, 1000, 1000, 6000] #[MHz] 
        self.CISPR22B_R = [3,3, 3, 3]
        self.CISPR22B_R_f = [30,1000,1000,6000]
        self.CISPR22B_E = np.interp(self.fdata,self.CISPR22B_E_f,self.CISPR22B_E)
        self.CISPR22B_RBW = np.interp(self.fdata,self.CISPR22B_RBW_f,self.CISPR22B_RBW)
        self.CISPR22B_R = np.interp(self.fdata,self.CISPR22B_R_f,self.CISPR22B_R) 
        self.CISPR22B_E = self.CISPR22B_E + 10*np.log10(self.CISPR22B_R/meas_dist)
        
        #CISPR11 Class A  >20KVA @10m intended for locations other than residential
        # frequencies above 1 GHz are interpolated same as CISPR22 class A
        self.CISPR11A_E_f = np.array([30,230,230,1000,1000,3000,3000,6000])
        self.CISPR11A_E = np.array([50, 50, 50, 50, 57,57,57, 57]) #dBuV/m
        self.CISPR11A_RBW = [120e3, 120e3, 1e6, 1e6]
        self.CISPR11A_RBW_f = [30, 1000, 1000, 6000] #[MHz]
        self.CISPR11A_R = [3, 3, 3, 3]
        self.CISPR11A_R_f = [30,1000,1000,6000]
        self.CISPR11A_E = np.interp(self.fdata,self.CISPR11A_E_f,self.CISPR11A_E)
        self.CISPR11A_RBW = np.interp(self.fdata,self.CISPR11A_RBW_f,self.CISPR11A_RBW)
        self.CISPR11A_R = np.interp(self.fdata,self.CISPR11A_R_f,self.CISPR11A_R) 
        self.CISPR11A_E = self.CISPR11A_E + 10*np.log10(self.CISPR11A_R/meas_dist)

        
        #CISPR11 Class B  >20KVA @10m
        # frequencies above 1 GHz are interpolated same as CISPR22 class A
        self.CISPR11B_E_f = np.array([30,230,230,1000,1000,3000,3000,6000])
        self.CISPR11B_E = np.array([30, 30, 37, 37, 50,50,54, 54]) #dBuV/m
        self.CISPR11B_RBW = [120e3, 120e3, 1e6, 1e6]
        self.CISPR11B_RBW_f = [30, 1000, 1000, 6000] #[MHz]
        self.CISPR11B_R = [10, 10, 3, 3]
        self.CISPR11B_R_f = [30,1000,1000,6000]
        self.CISPR11B_E = np.interp(self.fdata,self.CISPR11B_E_f,self.CISPR11B_E)
        self.CISPR11B_RBW = np.interp(self.fdata,self.CISPR11B_RBW_f,self.CISPR11B_RBW)
        self.CISPR11B_R = np.interp(self.fdata,self.CISPR11B_R_f,self.CISPR11B_R) 
        self.CISPR11B_E = self.CISPR11B_E + 10*np.log10(self.CISPR11B_R/meas_dist)
        
        # EN62040 - UPS systems
        # frequencies 
        self.EN62040C1_E_f = np.array([30,230,230,1000,1000,3000,3000,6000])
        self.EN62040C1_E = np.array([30, 30, 37, 37, 37,37,37, 37]) #dBuV/m
        self.EN62040C1_RBW = [120e3, 120e3, 1e6, 1e6]
        self.EN62040C1_RBW_f = [30, 1000, 1000, 6000] #[MHz]
        self.EN62040C1_R = [10, 10, 10, 10]
        self.EN62040C1_R_f = [30,1000,1000,6000]
        self.EN62040C1_E = np.interp(self.fdata,self.EN62040C1_E_f,self.EN62040C1_E)
        self.EN62040C1_RBW = np.interp(self.fdata,self.EN62040C1_RBW_f,self.EN62040C1_RBW)
        self.EN62040C1_R = np.interp(self.fdata,self.EN62040C1_R_f,self.EN62040C1_R) 
        self.EN62040C1_E = self.EN62040C1_E + 10*np.log10(self.EN62040C1_R/meas_dist)
    
        # EN62040 - UPS systems
        # frequencies 
        self.EN62040C2_E_f = np.array([30,230,230,1000,1000,3000,3000,6000])
        self.EN62040C2_E = np.array([40, 40, 47, 47, 47,47,47, 47]) #dBuV/m
        self.EN62040C2_RBW = [120e3, 120e3, 1e6, 1e6]
        self.EN62040C2_RBW_f = [30, 1000, 1000, 6000] #[MHz]
        self.EN62040C2_R = [10, 10, 10, 10]
        self.EN62040C2_R_f = [30,1000,1000,6000]
        self.EN62040C2_E = np.interp(self.fdata,self.EN62040C2_E_f,self.EN62040C2_E)
        self.EN62040C2_RBW = np.interp(self.fdata,self.EN62040C2_RBW_f,self.EN62040C2_RBW)
        self.EN62040C2_R = np.interp(self.fdata,self.EN62040C2_R_f,self.EN62040C2_R) 
        self.EN62040C2_E = self.EN62040C2_E + 10*np.log10(self.EN62040C2_R/meas_dist)
        
        # EN62040 - UPS systems
        # frequencies 
        self.EN62040C3_E_f = np.array([30,230,230,1000,1000,3000,3000,6000])
        self.EN62040C3_E = np.array([50, 50, 60, 60, 60,60,60, 60]) #dBuV/m
        self.EN62040C3_RBW = [120e3, 120e3, 1e6, 1e6]
        self.EN62040C3_RBW_f = [30, 1000, 1000, 6000] #[MHz]
        self.EN62040C3_R = [10, 10, 10, 10]
        self.EN62040C3_R_f = [30,1000,1000,6000]
        self.EN62040C3_E = np.interp(self.fdata,self.EN62040C3_E_f,self.EN62040C3_E)
        self.EN62040C3_RBW = np.interp(self.fdata,self.EN62040C3_RBW_f,self.EN62040C3_RBW)
        self.EN62040C3_R = np.interp(self.fdata,self.EN62040C3_R_f,self.EN62040C3_R) 
        self.EN62040C3_E = self.EN62040C3_E + 10*np.log10(self.EN62040C3_R/meas_dist)
        
        # EN61000-6-4 - general EMC standard
        # frequencies 
        self.EN61000_6_4_E_f = np.array([30,230,230,1000,1000,3000,3000,6000])
        self.EN61000_6_4_E = np.array([40, 40, 47, 47, 76,76,80, 80]) #dBuV/m
        self.EN61000_6_4_RBW = [120e3, 120e3, 1e6, 1e6]
        self.EN61000_6_4_RBW_f = [30, 1000, 1000, 6000] #[MHz]
        self.EN61000_6_4_R = [10, 10, 3, 3]
        self.EN61000_6_4_R_f = [30,1000,1000,6000]
        self.EN61000_6_4_E = np.interp(self.fdata,self.EN61000_6_4_E_f,self.EN61000_6_4_E)
        self.EN61000_6_4_RBW = np.interp(self.fdata,self.EN61000_6_4_RBW_f,self.EN61000_6_4_RBW)
        self.EN61000_6_4_R = np.interp(self.fdata,self.EN61000_6_4_R_f,self.EN61000_6_4_R) 
        self.EN61000_6_4_E = self.EN61000_6_4_E + 10*np.log10(self.EN61000_6_4_R/meas_dist)


        
        #MIL-STD-461F Navy mobile & army
        self.MILSTD461_E_f = np.array([10,100,18e3,20e3])
        self.MILSTD461_E = np.array([24, 24, 69, 69])#dBuV/m
        self.MILSTD461_RBW = [10e3, 100e3, 1e6, 1e6]
        self.MILSTD461_RBW_f = [10, 1000, 1000, 20e3] #[MHz]
        self.MILSTD461_R = [1,1]
        self.MILSTD461_R_f = [10,20e3]
        self.MILSTD461_E = np.interp(np.log10(self.fdata),np.log10(self.MILSTD461_E_f),self.MILSTD461_E)
        self.MILSTD461_RBW = np.interp(self.fdata,self.MILSTD461_RBW_f,self.MILSTD461_RBW)
        self.MILSTD461_R = np.interp(self.fdata,self.MILSTD461_R_f,self.MILSTD461_R) 
        self.MILSTD461_E = self.MILSTD461_E + 10*np.log10(self.MILSTD461_R/meas_dist) 


        #CISPR12 automotive
        self.CISPR12_E_f = np.array([30,75,400,1e3,6e3])
        self.CISPR12_E = np.array([34,34, 45, 45,45])#dBuV/m
        self.CISPR12_RBW = [120e3, 120e3, 120e3, 120e3,1e6]
        self.CISPR12_RBW_f = np.array([30,75,400,1e3,1e3]) #[MHz]
        self.CISPR12_R = [10,10]
        self.CISPR12_R_f = [30,1e3]
        self.CISPR12_E = np.interp(np.log10(self.fdata),np.log10(self.CISPR12_E_f),self.CISPR12_E)
        self.CISPR12_RBW = np.interp(self.fdata,self.CISPR12_RBW_f,self.CISPR12_RBW)
        self.CISPR12_R = np.interp(self.fdata,self.CISPR12_R_f,self.CISPR12_R) 
        self.CISPR12_E = self.CISPR12_E + 10*np.log10(self.CISPR12_R/meas_dist) 


    
        if name == 'CISPR22A':
            self.E = self.CISPR22A_E
            self.R = self.CISPR22A_R
            self.RBW = self.CISPR22A_RBW
        
        if name == 'TX_MRS':
            self.E = self.TX_MRS_E
            self.R = self.TX_MRS_R
            self.RBW = self.TX_MRS_RBW
            
        if name == 'CISPR22B':
            self.E = self.CISPR22B_E
            self.R = self.CISPR22B_R
            self.RBW = self.CISPR22B_RBW
        
        if name == 'CISPR11A':
            self.E = self.CISPR11A_E
            self.R = self.CISPR11A_R
            self.RBW = self.CISPR11A_RBW
        if name == 'CISPR11B':
            self.E = self.CISPR11B_E
            self.R = self.CISPR11B_R
            self.RBW = self.CISPR11B_RBW
        if name == 'MILSTD461':
            self.E = self.MILSTD461_E
            self.R = self.MILSTD461_R
            self.RBW = self.MILSTD461_RBW

        if name == 'CISPR12':
            self.E = self.CISPR12_E
            self.R = self.CISPR12_R
            self.RBW = self.CISPR12_RBW

        if name == 'EN62040C1':
            self.E = self.EN62040C1_E
            self.R = self.EN62040C1_R
            self.RBW = self.EN62040C1_RBW

        if name == 'EN62040C2':
            self.E = self.EN62040C2_E
            self.R = self.EN62040C2_R
            self.RBW = self.EN62040C2_RBW


        if name == 'EN62040C3':
            self.E = self.EN62040C3_E
            self.R = self.EN62040C3_R
            self.RBW = self.EN62040C3_RBW

        if name == 'EN61000_6_4':
            self.E = self.EN61000_6_4_E
            self.R = self.EN61000_6_4_R
            self.RBW = self.EN61000_6_4_RBW

        self.EIRP = self.E - 120 + 10*np.log10(4*np.pi*self.R**2/120/np.pi) + 30    
        
        
    def plot_stds(self):
        plt.figure(figsize=[20,15])
        ax = plt.axes()
        plt.semilogx(self.fdata,self.CISPR22A_E,linewidth=4)
        plt.semilogx(self.fdata,self.CISPR22B_E,linewidth=4)
        plt.semilogx(self.fdata,self.CISPR11A_E,linewidth=4)
        plt.semilogx(self.fdata,self.CISPR11B_E,linewidth=4)
        plt.semilogx(self.fdata,self.MILSTD461_E,linewidth=4)
        plt.semilogx(self.fdata,self.CISPR12_E,linewidth=4)
        ax.grid('on','Both')
        plt.xlabel('MHz')
        plt.ylabel('dBuV/m')
        plt.legend(['CISPR22A','CISPR22B','CISPR11A','CISPR11B','MIL-STD-461','CISPR12'])
        
        if self.name !='':
          plt.figure()
          ax = plt.axes()
          plt.semilogx(self.fdata,self.E)
          plt.title(self.name)
        

if __name__ == '__main__':
    
    import matplotlib 
    
    font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 30}
    matplotlib.rc('font', **font)

    fmin = 10
    fmax = 20e3
    N = 1000
    meas_dist = 10
    std = stds(np.arange(0,fmax,fmax/N),meas_dist,'MILSTD461')
    std.plot_stds()


    
        