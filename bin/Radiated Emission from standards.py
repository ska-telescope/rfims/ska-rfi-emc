# -*- coding: utf-8 -*-
"""
Created on Mon Jul  8 16:04:36 2019

@author: f.divruno
"""
import numpy as np
import matplotlib.pyplot as plt
from EMC_library import stds

import matplotlib 

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 30}
matplotlib.rc('font', **font)


freq = np.logspace(1,4.3,2000)
meas_dist = 10

# CISPR 32 class A
# Quasi peak below 1 Ghz
# Average above 1 GHz
# Semi Anechoic Chamber @3m
CISPR32A_E_f = np.array([30,230,230,1000,1000,30000,3000,6000])
CISPR32A_E = np.array([50, 50, 57, 57, 56,56,60, 60]) #dBuV/m
CISPR32A_RBW = [120e3, 120e3, 1e6, 1e6]
CISPR32A_RBW_f = [30, 1000, 1000, 6000] #[MHz] 
CISPR32A_R = [3,3, 3, 3]
CISPR32A_R_f = [30,1000,1000,6000]
CISPR32A_E = np.interp(freq,CISPR32A_E_f,CISPR32A_E)
CISPR32A_RBW = np.interp(freq,CISPR32A_RBW_f,CISPR32A_RBW)
CISPR32A_R = np.interp(freq,CISPR32A_R_f,CISPR32A_R) 
CISPR32A_E = CISPR32A_E + 20*np.log10(CISPR32A_R/meas_dist)
#in Jy with Tx at 1000km
CISPR32A_Jy = CISPR32A_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz



# CISPR 32 class B
# Quasi peak below 1 Ghz
# Average above 1 GHz
# Semi Anechoic Chamber @3m
CISPR32B_E_f = np.array([30,230,230,1000,1000,30000,3000,6000])
CISPR32B_E = np.array([40, 40, 47, 47, 50,50,54, 54]) #dBuV/m
CISPR32B_RBW = [120e3, 120e3, 1e6, 1e6]
CISPR32B_RBW_f = [30, 1000, 1000, 6000] #[MHz] 
CISPR32B_R = [3,3, 3, 3]
CISPR32B_R_f = [30,1000,1000,6000]
CISPR32B_E = np.interp(freq,CISPR32B_E_f,CISPR32B_E)
CISPR32B_RBW = np.interp(freq,CISPR32B_RBW_f,CISPR32B_RBW)
CISPR32B_R = np.interp(freq,CISPR32B_R_f,CISPR32B_R) 
CISPR32B_E = CISPR32B_E + 20*np.log10(CISPR32B_R/meas_dist)
#in Jy with Tx at 1000km
CISPR32B_Jy = CISPR32B_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz


# EN61000-6-4
# Quasi peak below 1 Ghz
# Average above 1 GHz
# Semi Anechoic Chamber @10m

EN61000_E_f = np.array([30,230,230,1000,1000,30000,3000,6000])
EN61000_E = np.array([40, 40, 47, 47, 50,50,54, 54]) #dBuV/m
EN61000_RBW = [120e3, 120e3, 1e6, 1e6]
EN61000_RBW_f = [30, 1000, 1000, 6000] #[MHz] 
EN61000_R = [10,10, 10, 10]
EN61000_R_f = [30,1000,1000,6000]
EN61000_E = np.interp(freq,EN61000_E_f,EN61000_E)
EN61000_RBW = np.interp(freq,EN61000_RBW_f,EN61000_RBW)
EN61000_R = np.interp(freq,EN61000_R_f,EN61000_R) 
EN61000_E = EN61000_E + 20*np.log10(EN61000_R/meas_dist)
#in Jy with Tx at 1000km
EN61000_Jy = EN61000_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz



# RTCA/ DO-160
# 
# peak detector
# Semi Anechoic Chamber @10m

DO160_E_f = np.array([100,108,108,152,152,320,320,340,340,960,960,
                     1164,1164,1215,1215,1525,1525,1559,1559,1610,
                     1610,1680,1680,5020,5020,5100,5100,6000])
DO160_E = np.array([0,0,25,27.5,0,0,37.7,38.1,0,0,45.3,47,38,38.5,0,
                    0,48.5,48.65,40,40.2,48.87,49.17,0,0,56.8,56.9,0,0]) #dBuV/m

DO160_E_baseline = 12.682 + 15.965*np.log10(freq)
DO160_RBW = [120e3, 120e3, 1e6, 1e6]
DO160_RBW_f = [30, 1000, 1000, 6000] #[MHz] 
DO160_R = [1,1, 1, 1]
DO160_R_f = [30,1000,1000,6000]
DO160_E = np.interp(np.log10(freq),np.log10(DO160_E_f),DO160_E)
DO160_E_mask = np.copy(DO160_E)
DO160_E_mask[~(DO160_E==0)] = 0
DO160_E_mask[(DO160_E==0)] = 1
DO160_E =  DO160_E + DO160_E_baseline*DO160_E_mask
DO160_RBW = np.interp(np.log10(freq),np.log10(DO160_RBW_f),DO160_RBW)
DO160_R = np.interp(np.log10(freq),np.log10(DO160_R_f),DO160_R) 
DO160_E = DO160_E + 20*np.log10(DO160_R/meas_dist)
#in Jy with Tx at 1000km
DO160_Jy = DO160_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz



# MIL-STD-461
# 
# peak detector
# Semi Anechoic Chamber @1m

MILSTD461_E_f = np.array([2,100,18000,20000])
MILSTD461_E = np.array([24,24,69,69]) #dBuV/m
MILSTD461_RBW = [100e3, 100e3, 1e6, 1e6]
MILSTD461_RBW_f = [2, 1000, 1000, 20000] #[MHz] 
MILSTD461_R = [1,1, 1, 1]
MILSTD461_R_f = [2,1000,1000,20000]
MILSTD461_E = np.interp(np.log10(freq),np.log10(MILSTD461_E_f),MILSTD461_E)
MILSTD461_RBW = np.interp(np.log10(freq),np.log10(MILSTD461_RBW_f),MILSTD461_RBW)
MILSTD461_R = np.interp(np.log10(freq),np.log10(MILSTD461_R_f),MILSTD461_R) 
MILSTD461_E = MILSTD461_E + 20*np.log10(MILSTD461_R/meas_dist)
#in Jy with Tx at 1000km
MILSTD461_Jy = MILSTD461_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz


# ECSS-E-ST-20-7C
# 
# peak detector
# Semi Anechoic Chamber @1m

ECSS_E_f = np.array([30,100,18000])
ECSS_E = np.array([60,60,90]) #dBuV/m
ECSS_RBW = [100e3, 100e3, 1e6, 1e6]
ECSS_RBW_f = [2, 1000, 1000, 20000] #[MHz] 
ECSS_R = [1,1, 1, 1]
ECSS_R_f = [2,1000,1000,20000]
ECSS_E = np.interp(np.log10(freq),np.log10(ECSS_E_f),ECSS_E)
ECSS_RBW = np.interp(np.log10(freq),np.log10(ECSS_RBW_f),ECSS_RBW)
ECSS_R = np.interp(np.log10(freq),np.log10(ECSS_R_f),ECSS_R) 
ECSS_E = ECSS_E + 20*np.log10(ECSS_R/meas_dist)

#in Jy with Tx at 1000km
ECSS_Jy = ECSS_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz

# MSFC-SPEC-521 
# 
# peak detector
# Semi Anechoic Chamber @1m

MSFC_E_f = np.array([200,18000,30000])
MSFC_E = np.array([30,65,65]) #dBuV/m
MSFC_RBW = [100e3, 100e3, 1e6, 1e6]
MSFC_RBW_f = [2, 1000, 1000, 20000] #[MHz] 
MSFC_R = [1,1, 1, 1]
MSFC_R_f = [2,1000,1000,20000]
MSFC_E = np.interp(np.log10(freq),np.log10(MSFC_E_f),MSFC_E)
MSFC_RBW = np.interp(np.log10(freq),np.log10(MSFC_RBW_f),MSFC_RBW)
MSFC_R = np.interp(np.log10(freq),np.log10(MSFC_R_f),MSFC_R) 
MSFC_E = MSFC_E + 20*np.log10(MSFC_R/meas_dist)

#in Jy with Tx at 1000km
MSFC_Jy = MSFC_E - 10*np.log10(120*np.pi *120e3) + 10*np.log10(meas_dist**2/1000e3**2) - 120 + 260 # dBW/m2/Hz




#
#%%plot
plt.figure(figsize=[20,15])
ax = plt.subplot(111)
ax2 = plt.subplot(111)

ax.semilogx(freq, CISPR32B_E, linewidth=5)
ax.semilogx(freq, EN61000_E, linewidth=5)
ax.semilogx(freq, MILSTD461_E, linewidth=5)
ax.semilogx(freq, ECSS_E, linewidth=5)
ax.semilogx(freq, MSFC_E, linewidth=5)
ax.semilogx(freq, DO160_E, linewidth=5)
ax.grid('on','Both')
ax.set_xlabel('MHz')
ax.set_ylabel('dBuV/m @' + str(meas_dist)+' m')
plt.legend(['CISPR32B','EN 61000-6-4','MIL-STD-461 space','ECSS-E-ST-20-07C','MSFC-SPEC-521C','DO-160'])

ax2 = ax.twinx()
ax2.yaxis.tick_right()
ax2.semilogx(freq, CISPR32B_Jy, linewidth=5)
ax2.semilogx(freq, EN61000_Jy, linewidth=5)
ax2.semilogx(freq, MILSTD461_Jy, linewidth=5)
ax2.semilogx(freq, ECSS_Jy, linewidth=5)
ax2.semilogx(freq, MSFC_Jy, linewidth=5)
ax2.semilogx(freq, DO160_Jy, linewidth=5)
ax2.set_ylabel('dBJy @1000 km')

plt.savefig('Compare radiated emissions standards.png')


