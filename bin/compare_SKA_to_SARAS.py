# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 23:35:47 2018
Compare MeerKat protection levels with SKA protection levels.
@author: f.divruno
"""


import numpy as np
import matplotlib.pyplot as plt
from math import * 
import matplotlib

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
matplotlib.rc('font', **font)

#%% 
# Define the frequency vector
Npoints = 1000 # Number of points to represent the frequency
freq_SKA = np.logspace(np.log10(50),np.log10(20000),Npoints) #[MHz]
freq_SARAS = np.logspace(np.log10(100),np.log10(25500),Npoints) #[MHz]

#
#%% SKA Standard Limits definitions

# Continuum limit line in PSD
SKA_Continuum_PSD =-17*np.log10(freq_SKA[freq_SKA<2000])-192 #[dBm/Hz]
SKA_Continuum_PSD = np.append(SKA_Continuum_PSD,-249*np.ones(np.size(freq_SKA[freq_SKA>=2000])))
RBW_Continuum = freq_SKA*1e6*1/100
SKA_Continuum_pow =  SKA_Continuum_PSD + 10*np.log10(RBW_Continuum)

# SARAS limit line in PSD for SARAS Levels
SARAS_PSD =-17.2708*np.log10(freq_SARAS[freq_SARAS<2000])-197.0714 #[dBm/Hz]
SARAS_PSD = np.append(SARAS_PSD,-253.8661*np.ones(np.size(freq_SARAS[freq_SARAS>=2000])))
RBW_SARAS = freq_SARAS*1e6*1/100
SARAS_pow =  SARAS_PSD + 10*np.log10(RBW_SARAS)

# Spectral line treshold
SKA_Line_PSD = -17*np.log10(freq_SKA[freq_SKA<2000])-177 #[dBm/Hz]
SKA_Line_PSD = np.append(SKA_Line_PSD,-234*np.ones(np.size(freq_SKA[freq_SKA>=2000])))
RBW_Line = freq_SKA*1e6*0.001/100
SKA_Line_pow =  SKA_Line_PSD + 10*np.log10(RBW_Line)


plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq_SKA,SKA_Continuum_PSD, label = 'Continuum PSD', linewidth=3)
plt.xlabel("frequency [MHz]")
plt.ylabel("PSD [dBm/Hz]")
plt.xscale('log')
L2, = plt.plot(freq_SKA,SKA_Line_PSD, label = 'Spectral Line PSD', linewidth=3)
L3, = plt.plot(freq_SARAS,SARAS_PSD, label = 'SARAS limit PSD', linewidth=3)
plt.grid(True,'both')
plt.title('SKA and SARAS Power Spectral Density Limits [dBm/Hz] received by the antenna')
#plt.title('SKA Power Spectral Density Limits [dBm/Hz] received by the antenna')
plt.legend()


#%% Power received

plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq_SKA,SKA_Continuum_pow, label = 'Continuum pow')
plt.xlabel("frequency [MHz]")
plt.ylabel("Power [dBm]")
plt.xscale('log')
L2, = plt.plot(freq_SKA,SKA_Line_pow, label = 'Spectral Line pow')
L3, = plt.plot(freq_SARAS,SARAS_pow, label = 'SARAS limit pow')
plt.grid(True,'both')
plt.title('SKA and SARAS Power [dBm/Hz] received by the antenna')
plt.legend()

#%% Electric field inciding the feed

E_Cont_SKA = SKA_Continuum_pow + 10*np.log10(120*np.pi**2*4)- 20*np.log10(3e8/freq_SKA/1e6) + 120
E_Line_SKA = SKA_Line_pow + 10*np.log10(120*np.pi**2*4)- 20*np.log10(3e8/freq_SKA/1e6) + 120

plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
plt.plot(freq_SKA,E_Cont_SKA, label = 'Continuum Incident E field')
plt.xlabel("frequency [MHz]")
plt.ylabel("Efield [dBuV/m]")
plt.xscale('log')
plt.plot(freq_SKA,E_Line_SKA, label = 'Line Incident E field')
plt.grid(True,'both')
plt.title('SKA received E field ')
plt.legend()



#%% Scaling the results by the measurement bandwith

RBW_Meas = 1e6*np.ones(np.size(freq_SARAS[freq_SARAS<1000]))
RBW_Meas = np.append(RBW_Meas,1000e3*np.ones(np.size(freq_SARAS[freq_SARAS>=1000])))

# Considering the emissions are narrowband, the spect line limit would be the same for All different RBWs.
# The Continuum line limit would decrease if the RX BW is greater than the Meas BW. This means that emissions 
# would add up once the RBW starts to increase in the receptor.

# look for the smallest RBW for continuum emissions.
RBW1 = RBW_Meas
RBW2 = RBW_Continuum
RBW_scale = np.ones(np.size(RBW1))
for i,val in enumerate(RBW_Continuum):
    if RBW1[i] > RBW2[i]:
        RBW_scale[i] = 1  # If the measurement RBW is greater than the RX RBW considers narrowband emissions and no scaling.
    else:
        RBW_scale[i] = (RBW1[i]/RBW2[i]) # If RBW Meas is smaller than RX RBW, lowers the limit according to the ratio.
SKA_Continuum_pow = SKA_Continuum_pow + 10*np.log10(RBW_scale)

RBW1 = RBW_Meas
RBW2 = RBW_SARAS
RBW_scale = np.ones(np.size(RBW1))
for i,val in enumerate(RBW_Continuum):
    if RBW1[i] > RBW2[i]:
        RBW_scale[i] = 1  # If the measurement RBW is greater than the RX RBW considers narrowband emissions and no scaling.
    else:
        RBW_scale[i] = (RBW1[i]/RBW2[i]) # If RBW Meas is smaller than RX RBW, lowers the limit according to the ratio.
SARAS_pow = SARAS_pow + 10*np.log10(RBW_scale)


SKA_Line_pow = SKA_Line_pow
#SKA_Continuum_pow = SKA_Continuum_PSD + 10*np.log10(RBW_Meas)
#SARAS_pow = SARAS_PSD + 10*np.log10(RBW_Meas)

plt.figure(figsize=(10, 7), dpi=80, facecolor='w', edgecolor='k')
L1, = plt.plot(freq_SKA,SKA_Continuum_pow, label = 'Continuum PSD + 10*log(B_meas)')
plt.xlabel("frequency [MHz]")
plt.ylabel("PSD [dBm]")
plt.xscale('log')
L2, = plt.plot(freq_SKA,SKA_Line_pow, label = 'Spectral Line pow')
L3, = plt.plot(freq_SARAS,SARAS_pow, label = 'SARAS limit PSD + 10*log(B_meas)')
plt.grid(True,'both')
plt.title('SKA and SARAS Power [dBm/Hz] received by the antenna, scaled by the Meas RBW.')
plt.legend()









