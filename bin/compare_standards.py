"""
    Author      : Dr. A. J. Otto
    Description : Comparison of SKA EMC Standards to Various
                  Commercial and Military EMC Standards
    Date        : 2 November 2020
"""
import matplotlib.pyplot as plt
import numpy as np
import time
import os


os.system("clear")


def channel_integration(freq, spectrum, savefile):
    """
        :description
        ____________
            Function to channel integrate measured data at
            measured RBW to equivalent Continuum (1%.fc) RBW
        :params
        _______
            freq      :: Frequency Data in [MHz]
            spectrum  :: Measured Power Data [dBm]
            savefile  :: Filename to Save Processed Data (String)
        :returns
        ________
            freqm   ::
            PSD_int ::
    """
    start_time = time.time()
    freqm, PSD_int = [], []
    filename = savefile
    with open(filename, 'w') as myfile:
        for a in range(0, len(freq)):
            fc = freq[a] * 1e6
            RBWc = 1./100 * fc
            f_start = np.argmin(abs(np.array(freq)*1e6 - (fc-RBWc/2)))
            f_stop = np.argmin(abs(np.array(freq)*1e6 - (fc+RBWc/2)))
            f_centre = np.argmin(abs(np.array(freq)*1e6 - fc))
            freqm.append(freq[f_centre])

            PSDi =\
                10.*np.log10(
                    np.sum(10.**(np.array(spectrum)[f_start:f_stop]/10.)*1e-3)
                    / 1e-3)\
                - 10.*np.log10(RBWc)
            PSD_int.append(PSDi)
            myfile.write(str(freq[f_centre]) + "," + str(PSDi) + "\n")
            # Not needed when not ran in Ipython
            # clear_output(wait=True)
            time.sleep(0.001)
            print(round(a/len(freq) * 100, 2), " % complete\r", end='')
    myfile.close()
    print("Completed in...", time.time() - start_time, " seconds")
    return freqm, PSD_int


def read_PSD(filename):
    """
    """
    freqm, PSDm = [], []
    try:
        for line in open(filename):
            tmp = line.split(',')
            freqm.append(float(tmp[0]))
            PSDm.append(float(tmp[1]))
        return freqm, PSDm
    except FileNotFoundError:
        print("Channel integration file not found...")
        return None, None


# CONSTANTS
eps0 = 8.854E-12  # F/m
mu0 = 4*np.pi*1E-7  # H/m
c0 = 1./np.sqrt(eps0*mu0)
Z0 = np.sqrt(mu0/eps0)

########################################
# SKA (SARAS) STANDARD :: PSD [dBm/Hz] #
########################################
# FREQUENCY [MHz]
f_SARAS = []
f_low = np.arange(50., 2000., 10.)
f_high = np.arange(2000., 22500., 10.)
f_SARAS.extend(f_low)
f_SARAS.extend(f_high)

# CONTINUUM PSD [dBm/Hz]
SARAS = []
SARAS_low = -17.2708 * np.log10(f_low) - 192.0714
SARAS_high = -0.065676 * np.log10(f_high) - 248.8661
SARAS.extend(SARAS_low)
SARAS.extend(SARAS_high)
# CONTINUUM RBW = 1%.fc
RBW = 1./100. * np.array(f_SARAS) * 1E6

# SPECTRAL LINE PSD [dBm/Hz]
SARAS_spec = np.array(SARAS) + 15.
# SPECTRAL LINE RBW = 0.001%.fc
RBW_spec = 0.001/100. * np.array(f_SARAS) * 1E6

# THRESHOLD DISTANCE (R_thresh)
R_thresh = 10.  # m
# MEASUREMENT DISTANCE (D_meas)
D_meas = 1.  # m

# FREE SPACE PATH LOSS [dB] at R_thresh
FSPL = 10.*np.log10(((4*np.pi*R_thresh)/(c0/(np.array(f_SARAS)*1E6)))**2.)

# PSD THRESHOLDS at R_thresh
PSD_cont_thresh = SARAS + FSPL
PSD_spec_thresh = SARAS_spec + FSPL

# E-FIELD EQUIVALENT CONTINUUM measured at D_meas
E_cont =\
    20.*np.log10(
        np.sqrt((10.**(PSD_cont_thresh/10.) * 1E-3 * RBW * Z0) /
                (4*np.pi*D_meas**2.))/1E-6)
# E-FIELD EQUIVALENT SPECTRAL LINE [dBuV/m] measured at D_meas
E_spec =\
    20.*np.log10(
        np.sqrt((10.**(PSD_spec_thresh/10.) * 1E-3 * RBW_spec * Z0) /
                (4*np.pi*D_meas**2.))/1E-6)
########################################

################################
# MIL-STD461F [dBuV/m] at D=1m #
################################
MIL_freq, MIL_psd = [], []
# 10 MHz < f < 100 MHz:
mil_freq1 = np.linspace(10, 100, 10)
navy1 = 44 * np.ones(len(mil_freq1)) + 20.*np.log(1./D_meas)
navy1_eirp = navy1 + 20.*np.log10(D_meas) - 104.8
freqm, PSDm = read_PSD('navy1.csv')
if freqm is None:
    freq_interp = np.arange(mil_freq1[0], mil_freq1[-1], 0.1)
    eirp_interp = np.interp(freq_interp, mil_freq1, navy1_eirp)
    freqm, PSDm = channel_integration(freq_interp, eirp_interp, 'navy1.csv')
# f < 1 GHz :: RBW = 100 kHz
navy1_psd = navy1_eirp - 10.*np.log10(100e3)
# FSPL = 10.*np.log10(((4*np.pi*D_meas)/(c0/(np.array(freqm)*1E6)))**2.)
MIL_freq.extend(freqm)
MIL_psd.extend(PSDm)

# 100 MHz < f < 18000 MHz:
mil_freq2 = [100, 200, 1000, 1001, 2000, 18000]
navy2 = [44, 50, 64, 64.1, 70, 89] + 20.*np.log10(1./D_meas)
navy2_eirp = navy2 + 20.*np.log10(D_meas) - 104.8
freqm, PSDm = read_PSD('navy2.csv')
if freqm is None:
    tmp_freq1 = np.arange(mil_freq2[0], 1000, 0.1)
    tmp_freq2 = np.arange(1001, mil_freq2[-1], 1)
    freq_interp = []
    freq_interp.extend(tmp_freq1)
    freq_interp.extend(tmp_freq2)
    eirp_interp = np.interp(freq_interp, mil_freq2, navy2_eirp)
    freqm, PSDm = channel_integration(freq_interp, eirp_interp, 'navy2.csv')
# f < 1 GHz :: RBW = 100 kHz
# f > 1 GHz :: RBW = 1 MHz
navy2_rbw = [100e3 if f <= 1000 else 1e6 for f in mil_freq2]
navy2_psd = navy2_eirp - 10.*np.log10(navy2_rbw)
MIL_freq.extend(freqm)
MIL_psd.extend(PSDm)

# 18 GHz < f < 20 GHz
mil_freq3 = [18000, 20000]
navy3 = [89, 89] + 20.*np.log10(1./D_meas)
navy3_eirp = navy3 + 20.*np.log10(D_meas) - 104.8
freqm, PSDm = read_PSD('navy3.csv')
if freqm is None:
    freq_interp = np.arange(mil_freq3[0], mil_freq3[-1], 1)
    eirp_interp = np.interp(freq_interp, mil_freq3, navy3_eirp)
    freqm, PSDm = channel_integration(freq_interp, eirp_interp, 'navy3.csv')
# f > 1 GHz :: RBW = 1 MHz
navy3_psd = navy3_eirp - 10.*np.log10(1e6)
MIL_freq.extend(freqm)
MIL_psd.extend(PSDm)
################################

################################
# FCC (Class A measured at D=10m)
################################
fcc_freq1 = np.linspace(30, 88, 10)
fcc_classA1 = 39 * np.ones(len(fcc_freq1)) + 20.*np.log10(10./D_meas)
fcc_classA1_eirp = fcc_classA1 + 20.*np.log10(D_meas) - 104.8
# f < 1 GHz :: RBW = 100 kHz
fcc_classA1_psd = fcc_classA1_eirp - 10.*np.log10(100e3)

fcc_freq2 = np.linspace(88, 216, 10)
fcc_classA2 = 44 * np.ones(len(fcc_freq2)) + 20.*np.log10(10./D_meas)
fcc_classA2_eirp = fcc_classA2 + 20.*np.log10(D_meas) - 104.8
# f < 1 GHz :: RBW = 100 kHz
fcc_classA2_psd = fcc_classA2_eirp - 10.*np.log10(100e3)

fcc_freq3 = np.linspace(216, 960, 10)
fcc_classA3 = 46 * np.ones(len(fcc_freq3)) + 20.*np.log10(10./D_meas)
fcc_classA3_eirp = fcc_classA3 + 20.*np.log10(D_meas) - 104.8
# f < 1 GHz :: RBW = 100 kHz
fcc_classA3_psd = fcc_classA3_eirp - 10.*np.log10(100e3)

fcc_freq4 = np.linspace(960, 20000, 10)
fcc_classA4 = 50 * np.ones(len(fcc_freq4)) + 20.*np.log10(10./D_meas)
fcc_classA4_eirp = fcc_classA4 + 20.*np.log10(D_meas) - 104.8
# f > 1 GHz :: RBW = 1 MHz
fcc_classA4_psd = fcc_classA4_eirp - 10.*np.log10(1e6)

################################
# FCC (Class B measured at D=3m)
################################
# fcc_classB1 = 40 * np.ones(len(fcc_freq1))
# fcc_classB2 = 44 * np.ones(len(fcc_freq2))
# fcc_classB3 = 46 * np.ones(len(fcc_freq3))
# fcc_classB4 = 54 * np.ones(len(fcc_freq4))
################################

######################################
# CISPR Class B [dBuV/m] at 10m and 3m
######################################
# f < 1 GHz :: RBW = 100 kHz
B_230 = 30 + 20.*np.log10(10./D_meas)  # 10m
B_230_eirp = B_230 + 20.*np.log10(D_meas) - 104.8
B_230_psd = B_230_eirp - 10.*np.log10(120e3)

# f < 1 GHz :: RBW = 100 kHz
B_1000 = 37 + 20.*np.log10(10./D_meas)  # 10m
B_1000_eirp = B_1000 + 20.*np.log10(D_meas) - 104.8
B_1000_psd = B_1000_eirp - 10.*np.log10(120e3)

# f > 1 GHz :: RBW = 1 MHz
B_3000_avg = 50 + 20.*np.log10(3./D_meas)  # 3m
B_3000_avg_eirp = B_3000_avg + 20.*np.log10(D_meas) - 104.8
B_3000_avg_psd = B_3000_avg_eirp - 10.*np.log10(1e6)

B_6000_avg = 54 + 20.*np.log10(3/D_meas)  # 3m
B_6000_avg_eirp = B_6000_avg + 20.*np.log10(D_meas) - 104.8
B_6000_avg_psd = B_6000_avg_eirp - 10.*np.log10(1e6)

B_3000_pk = 70 + 20.*np.log10(3./D_meas)  # 3m
B_3000_pk_eirp = B_3000_pk + 20.*np.log10(D_meas) - 104.8
B_3000_pk_psd = B_3000_pk_eirp - 10.*np.log10(1e6)

B_6000_pk = 74 + 20.*np.log10(3./D_meas)  # 3m
B_6000_pk_eirp = B_6000_pk + 20.*np.log10(D_meas) - 104.8
B_6000_pk_psd = B_6000_pk_eirp - 10.*np.log10(1e6)

######################################
# CISPR Class A [dBuV/m] at 10m and 3m
######################################
A_230 = 40 + 20.*np.log10(10./D_meas)  # 10m
A_230_eirp = A_230 + 20.*np.log10(D_meas) - 104.8
A_230_psd = A_230_eirp - 10.*np.log10(120e3)

A_1000 = 47 + 20.*np.log10(10./D_meas)  # 10m
A_1000_eirp = A_1000 + 20.*np.log10(D_meas) - 104.8
A_1000_psd = A_1000_eirp - 10.*np.log10(120e3)

A_3000_avg = 56 + 20.*np.log10(3./D_meas)  # 3m
A_3000_avg_eirp = A_3000_avg + 20.*np.log10(D_meas) - 104.8
A_3000_avg_psd = A_3000_avg_eirp - 10.*np.log10(1e6)

A_6000_avg = 60 + 20.*np.log10(3./D_meas)  # 3m
A_6000_avg_eirp = A_6000_avg + 20.*np.log10(D_meas) - 104.8
A_6000_avg_psd = A_6000_avg_eirp - 10.*np.log10(1e6)

A_3000_pk = 76 + 20.*np.log10(3./D_meas)  # 3m
A_3000_pk_eirp = A_3000_pk + 20.*np.log10(D_meas) - 104.8
A_3000_pk_psd = A_3000_pk_eirp - 10.*np.log10(1e6)

A_6000_pk = 80 + 20.*np.log10(3./D_meas)  # 3m
A_6000_pk_eirp = A_6000_pk + 20.*np.log10(D_meas) - 104.8
A_6000_pk_psd = A_6000_pk_eirp - 10.*np.log10(1e6)
######################################
######################################
######################################


################################
# DATA PLOT - E-FIELD [dBuV/m] #
################################
fig = plt.figure(figsize=(10, 6))
ax1 = fig.add_subplot(111)
# MIL-STD 461 (D=1m)
mil, = ax1.semilogx(mil_freq1, navy1, '-g')
ax1.semilogx(mil_freq2, navy2, '-g')
ax1.semilogx(mil_freq3, navy3, '-g')
# FCC (Class A D=10m; Class B D=3m)
fcc, = ax1.semilogx(fcc_freq1, fcc_classA1, '-k', alpha=0.75)
ax1.semilogx([fcc_freq2[0], fcc_freq2[0]],
             [fcc_classA1[-1], fcc_classA2[0]],
             '-k', alpha=0.75)
ax1.semilogx(fcc_freq2, fcc_classA2, '-k', alpha=0.75)
ax1.semilogx([fcc_freq3[0], fcc_freq3[0]],
             [fcc_classA2[-1], fcc_classA3[0]],
             '-k', alpha=0.75)
ax1.semilogx(fcc_freq3, fcc_classA3, '-k', alpha=0.75)
ax1.semilogx([fcc_freq4[0], fcc_freq4[0]],
             [fcc_classA3[-1], fcc_classA4[0]],
             '-k', alpha=0.75)
ax1.semilogx(fcc_freq4, fcc_classA4, '-k', alpha=0.75)
# CISPR (Class A D=10m and D=3m)
a, = plt.semilogx([30, 230], [B_230, B_230], 'b', alpha=0.5)
plt.semilogx([230, 230], [B_230, B_1000], 'b', alpha=0.5)
plt.semilogx([230, 1000], [B_1000, B_1000], 'b', alpha=0.5)
plt.semilogx([1000, 1000], [B_1000, B_3000_avg], 'b', alpha=0.5)
plt.semilogx([1000, 3000], [B_3000_avg, B_3000_avg], 'b', alpha=0.5)
plt.semilogx([3000, 3000], [B_3000_avg, B_6000_avg], 'b', alpha=0.5)
plt.semilogx([3000, 6000], [B_6000_avg, B_6000_avg], 'b', alpha=0.5)

b, = plt.semilogx([1000, 1000], [B_3000_avg, B_3000_pk], '--b', alpha=0.5)
plt.semilogx([1000, 3000], [B_3000_pk, B_3000_pk], '--b', alpha=0.5)
plt.semilogx([3000, 3000], [B_3000_pk, B_6000_pk], '--b', alpha=0.5)
plt.semilogx([3000, 6000], [B_6000_pk, B_6000_pk], '--b', alpha=0.5)

c, = plt.semilogx([30, 230], [B_230 + 10, B_230 + 10], '-r', alpha=0.5)
plt.semilogx([230, 230], [B_230 + 10, B_1000 + 10], '-r', alpha=0.5)
plt.semilogx([230, 1000], [B_1000 + 10, B_1000 + 10], '-r', alpha=0.5)
plt.semilogx([1000, 1000], [B_1000 + 10, B_3000_avg + 6], '-r', alpha=0.5)
plt.semilogx([1000, 3000], [B_3000_avg + 6, B_3000_avg + 6], '-r', alpha=0.5)
plt.semilogx([3000, 3000], [B_3000_avg + 6, B_6000_avg + 6], '-r', alpha=0.5)
plt.semilogx([3000, 6000], [B_6000_avg + 6, B_6000_avg + 6], '-r', alpha=0.5)

d, = plt.semilogx([1000, 1000], [B_3000_avg + 6, B_3000_pk + 6],
                  '--r', alpha=0.5)
plt.semilogx([1000, 3000], [B_3000_pk + 6, B_3000_pk + 6], '--r', alpha=0.5)
plt.semilogx([3000, 3000], [B_3000_pk + 6, B_6000_pk + 6], '--r', alpha=0.5)
plt.semilogx([3000, 6000], [B_6000_pk + 6, B_6000_pk + 6], '--r', alpha=0.5)
# SKA (SARAS) STANDARD
ska_cont, = ax1.semilogx(f_SARAS, E_cont, '-b')
ska_spec, = ax1.semilogx(f_SARAS, E_spec, '-r')
ska_rbw_comp = [100e3 if f <= 1000 else 1e6 for f in f_SARAS]
ska_comp, = ax1.semilogx(f_SARAS,
                         E_spec +
                         10.*np.log10(
                             (0.001/100.*np.array(f_SARAS)*1e6) /
                             (ska_rbw_comp)),
                         '--r')

plt.legend([mil, fcc, a, b, c, d, ska_cont, ska_spec, ska_comp],
           ['MIL-STD 461F', 'FCC Class A',
            'CISPR Class B (QP+Avg)', 'CISPR Class B (Pk)',
            'CISPR Class A (QP+Avg)', 'CISPR Class A (Pk)',
            'SKA Continuum 10m Threshold',
            'SKA Spectral Line 10m Threshold',
            'SKA Spectral Line 10m Threshold: Bandwidth Compensated'],
           loc='best', fontsize=8)
plt.grid(True, 'both', alpha=0.5)
plt.title('Radiated Emission Limits at a Distance of %i m from DUT' % D_meas)
plt.xlabel('Frequency [MHz]')
plt.ylabel('E-Field [dBuV/m]')
plt.tight_layout()

############################
# DATA PLOT - PSD [dBm/Hz] #
############################
fig = plt.figure(figsize=(10, 6))
ax1 = fig.add_subplot(111)
# MIL-STD 461 (D=1m)
mil, = ax1.semilogx(mil_freq1, navy1_psd, '-g')
ax1.semilogx(mil_freq2, navy2_psd, '-g')
ax1.semilogx(mil_freq3, navy3_psd, '-g')
# Channel Integrated - Continuum
mil_chan, = ax1.semilogx(MIL_freq, MIL_psd, '--g', alpha=0.5)

# FCC (Class A D=10m; Class B D=3m)
# fcc, = ax1.semilogx(fcc_freq1, fcc_classA1_psd, '-k', alpha=0.75)
# ax1.semilogx([fcc_freq2[0], fcc_freq2[0]],
#              [fcc_classA1_psd[-1], fcc_classA2_psd[0]],
#              '-k', alpha=0.75)
# ax1.semilogx(fcc_freq2, fcc_classA2_psd, '-k', alpha=0.75)
# ax1.semilogx([fcc_freq3[0], fcc_freq3[0]],
#              [fcc_classA2_psd[-1], fcc_classA3_psd[0]],
#              '-k', alpha=0.75)
# ax1.semilogx(fcc_freq3, fcc_classA3_psd, '-k', alpha=0.75)
# ax1.semilogx([fcc_freq4[0], fcc_freq4[0]],
#              [fcc_classA3_psd[-1], fcc_classA4_psd[0]],
#              '-k', alpha=0.75)
# ax1.semilogx(fcc_freq4, fcc_classA4_psd, '-k', alpha=0.75)

# CISPR (Class A D=10m and D=3m)
# a, = plt.semilogx([30, 230], [B_230_psd, B_230_psd], 'b', alpha=0.5)
# plt.semilogx([230, 230], [B_230_psd, B_1000_psd], 'b', alpha=0.5)
# plt.semilogx([230, 1000], [B_1000_psd, B_1000_psd], 'b', alpha=0.5)
# plt.semilogx([1000, 1000], [B_1000_psd, B_3000_avg_psd], 'b', alpha=0.5)
# plt.semilogx([1000, 3000], [B_3000_avg_psd, B_3000_avg_psd], 'b', alpha=0.5)
# plt.semilogx([3000, 3000], [B_3000_avg_psd, B_6000_avg_psd], 'b', alpha=0.5)
# plt.semilogx([3000, 6000], [B_6000_avg_psd, B_6000_avg_psd], 'b', alpha=0.5)
#
# b, = plt.semilogx([1000, 1000], [B_3000_avg_psd, B_3000_pk_psd],
#                   '--b', alpha=0.5)
# plt.semilogx([1000, 3000], [B_3000_pk_psd, B_3000_pk_psd], '--b', alpha=0.5)
# plt.semilogx([3000, 3000], [B_3000_pk_psd, B_6000_pk_psd], '--b', alpha=0.5)
# plt.semilogx([3000, 6000], [B_6000_pk_psd, B_6000_pk_psd], '--b', alpha=0.5)
#
# c, = plt.semilogx([30, 230], [A_230_psd, A_230_psd], 'r', alpha=0.5)
# plt.semilogx([230, 230], [A_230_psd, A_1000_psd], 'r', alpha=0.5)
# plt.semilogx([230, 1000], [A_1000_psd, A_1000_psd], 'r', alpha=0.5)
# plt.semilogx([1000, 1000], [A_1000_psd, A_3000_avg_psd], 'r', alpha=0.5)
# plt.semilogx([1000, 3000], [A_3000_avg_psd, A_3000_avg_psd], 'r', alpha=0.5)
# plt.semilogx([3000, 3000], [A_3000_avg_psd, A_6000_avg_psd], 'r', alpha=0.5)
# plt.semilogx([3000, 6000], [A_6000_avg_psd, A_6000_avg_psd], 'r', alpha=0.5)
#
# d, = plt.semilogx([1000, 1000], [A_3000_avg_psd, A_3000_pk_psd],
#                   '--r', alpha=0.5)
# plt.semilogx([1000, 3000], [A_3000_pk_psd, A_3000_pk_psd], '--r', alpha=0.5)
# plt.semilogx([3000, 3000], [A_3000_pk_psd, A_6000_pk_psd], '--r', alpha=0.5)
# plt.semilogx([3000, 6000], [A_6000_pk_psd, A_6000_pk_psd], '--r', alpha=0.5)

# SKA PSD THRESHOLDS at R_thresh
ska_cont, = ax1.semilogx(f_SARAS, PSD_cont_thresh, '-b')
ska_spec, = ax1.semilogx(f_SARAS, PSD_spec_thresh, '-r')
# RBWm Compensation
ska_rbw_comp = [100e3 if f <= 1000 else 1e6 for f in f_SARAS]
ska_comp, = ax1.semilogx(f_SARAS,
                         PSD_spec_thresh +
                         10.*np.log10(
                             (0.001/100.*np.array(f_SARAS)*1e6) /
                             (ska_rbw_comp)),
                         '--r')

# plt.legend([mil, fcc, a, b, c, d, ska_cont, ska_spec, ska_comp],
#            ['MIL-STD 461F', 'FCC Class A',
#             'CISPR Class B (QP+Avg)', 'CISPR Class B (Pk)',
#             'CISPR Class A (QP+Avg)', 'CISPR Class A (Pk)',
#             'SKA Continuum 10m Threshold',
#             'SKA Spectral Line 10m Threshold',
#             'SKA Spectral Line 10m Threshold: Bandwidth Compensated'],
#            loc='best', fontsize=8)
plt.legend([mil, mil_chan, ska_cont, ska_spec, ska_comp],
           ['MIL-STD 461F',
            'MIL-STD 461F: Channel Integrated (Continuum)',
            'SKA Continuum 10m Threshold',
            'SKA Spectral Line 10m Threshold',
            'SKA Spectral Line 10m Threshold: Bandwidth Compensated'],
           loc='best', fontsize=8)

plt.grid(True, 'both', alpha=0.5)
plt.title('Radiated Emission Limits at a Distance of %i m from DUT' % D_meas)
plt.xlabel('Frequency [MHz]')
plt.ylabel('PSD [dBm/Hz]')
plt.tight_layout()
plt.show()
